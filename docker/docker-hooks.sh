#!/bin/bash -e

[[ -z "$MIGRATIONS" ]] || { echo "Migrations.."; php ./console.php migrate --interactive=0 ; }
