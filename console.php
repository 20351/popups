<?php

declare(strict_types=1);

use GuzzleHttp\Client;
use Http\Transport\DefaultHttpTransport;
use Http\Transport\JsonDecoded;
use Infrastructure\Transport\CachedTransport;
use Popups\Infrastructure\Application\Config\Console as ConsoleConfig;
use Infrastructure\Environment\EnvironmentDependentEnvFile;
use Infrastructure\Logging\WithElkAndSentryLog;
use Infrastructure\Application\Database\MariaDb;
use Infrastructure\Application\Console;
use Infrastructure\Error\ErrorHandler;
use Popups\Infrastructure\Application\Config\KafkaHandlers;
use yii\console\ExitCode;
use yii\helpers\Console as ConsoleHelper;

require_once __DIR__ . '/vendor/autoload.php';

(new EnvironmentDependentEnvFile(__DIR__, getallheaders()))->load();

define('YII_DEBUG', getenv('YII_DEBUG') === 'true');
define('YII_ENABLE_ERROR_HANDLER', false); // need to use our `set_error_handler`, `set_exception_handler`

(new ErrorHandler())->set();

set_exception_handler(
    function (Throwable $throwable) {
        (new WithElkAndSentryLog())->run($throwable);

        echo
        sprintf(
            '%s %s' . PHP_EOL . '%s %s' . PHP_EOL . '%s %s' . PHP_EOL . PHP_EOL . '%s' . PHP_EOL . '%s' . PHP_EOL,
            ConsoleHelper::renderColoredString('%RException%n:', true),
            $throwable->getMessage(),
            ConsoleHelper::renderColoredString('%RFile%n:', true),
            $throwable->getFile(),
            ConsoleHelper::renderColoredString('%RLine%n:', true),
            $throwable->getLine(),
            ConsoleHelper::renderColoredString('%CTrace%n:', true),
            $throwable->getTraceAsString()
        );
        exit(ExitCode::UNSPECIFIED_ERROR);
    }
);

require_once __DIR__ . '/vendor/yiisoft/yii2/Yii.php';

$application =
    (new Console(
        new ConsoleConfig(
            new MariaDb(),
            new KafkaHandlers(),
        )
    ))
        ->application();

//todo вынести настройку контейнера в единое место

Yii::$container->setSingleton(
    'transport',
    new JsonDecoded(new DefaultHttpTransport(new Client()))
);

Yii::$container->setSingleton(
    'cachedTransport',
    new CachedTransport(
        new JsonDecoded(new DefaultHttpTransport(new Client())),
        Yii::$app->cache,
        60
    )
);

$exitCode = $application->run();

exit($exitCode);