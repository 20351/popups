<?php

declare(strict_types=1);

use Infrastructure\Environment\EnvironmentDependentEnvFile;
use Infrastructure\Logging\WithElkAndSentryLog;
use Infrastructure\Error\ErrorHandler;
use Infrastructure\Error\JsonExceptionHandler;
use Popups\Infrastructure\Application\TypeHinting;
use yii\BaseYii;

header ("Access-Control-Allow-Origin: *");
header ("Access-Control-Expose-Headers: Content-Length, X-JSON");
header ("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
header ("Access-Control-Allow-Headers: *");

require_once __DIR__ . '/../vendor/autoload.php';

(new EnvironmentDependentEnvFile(dirname(__DIR__), getallheaders()))->load();

define('YII_DEBUG', getenv('YII_DEBUG') === 'true');
define('YII_ENABLE_ERROR_HANDLER', false); // need to use our `set_error_handler`, `set_exception_handler`

(new ErrorHandler())->set();

(new JsonExceptionHandler(
    new WithElkAndSentryLog()
))
    ->set();

require_once __DIR__ . '/../vendor/yiisoft/yii2/BaseYii.php';

class Yii extends BaseYii
{
    /**
     * @var TypeHinting hack for component type hinting
     */
    public static $app;
}

spl_autoload_register(['Yii', 'autoload'], true, true);
Yii::$classMap = require __DIR__ . '/../vendor/yiisoft/yii2/classes.php';
Yii::$container = new yii\di\Container();
