<?php

declare(strict_types=1);

use Infrastructure\Application\Database\MariaDb;
use Infrastructure\Application\Language\Russian;
use Infrastructure\Application\Web;
use Infrastructure\Application\Routes\MergedRoutes;
use Popups\Infrastructure\Application\Config\Restful as RestfulConfig;
use Popups\Infrastructure\Application\System\ApiV1;
use Popups\Models\Identity\ByAppToken;
use Popups\Routes\Api\ApiV1 as ApiV1Routes;

require_once __DIR__ . '/../bootstrap.php';

(new Web(
    new RestfulConfig(
        new ApiV1(),
        new MariaDb(),
        new Russian(),
        new MergedRoutes(
            new ApiV1Routes()
        ),
        new ByAppToken()
    )
))
    ->application()
    ->run();
