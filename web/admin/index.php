<?php

declare(strict_types=1);

use Popups\Infrastructure\Application\Config\Restful as RestfulConfig;
use Infrastructure\Application\Database\MariaDb;
use Infrastructure\Application\Language\Russian;
use Infrastructure\Application\Web;
use Popups\Infrastructure\Application\System\Admin;
use Popups\Models\Identity\ByAccessToken;
use Popups\Routes\Admin as AdminRoutes;

require_once __DIR__ . '/../bootstrap.php';

(new Web(
    new RestfulConfig(
        new Admin(),
        new MariaDb(),
        new Russian(),
        new AdminRoutes(),
        new ByAccessToken()
    )
))
    ->application()
    ->run();
