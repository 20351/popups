<?php

declare(strict_types=1);

use Infrastructure\Application\Database\MariaDb;
use Infrastructure\Application\Language\Russian;
use Infrastructure\Application\Web;
use Popups\Infrastructure\Application\Config\Html;
use Popups\Infrastructure\Application\System\Common;
use Popups\Routes\Common as CommonRoutes;

require_once __DIR__ . '/../bootstrap.php';

(new Web(
    new Html(
        new Common(),
        new MariaDb(),
        new Russian(),
        new CommonRoutes()
    )
))
    ->application()
    ->run();
