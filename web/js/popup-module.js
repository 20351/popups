var newDiv = document.createElement("div");
newDiv.id = "popups";
newDiv.innerHTML = "<popup-component />";

document.body.appendChild(newDiv);

import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.esm.browser.js'

Vue.component('popup-component', {
   template: `
        <div :class="{'popup-component__overlay': getPopupBlackout}">
            <div
                v-for="(popup, index) in popupList"
                :key="index"
                class="popup-component toast"
                :class="'popup-component--' + positionMap[popup.position]">
                <div
                    v-if="popup.position == 'right_bottom' || popup.position == 'right_top'"
                    class="toast"
                    :class="{'toast--white': popup.style == 'white'}">
                    <span v-if="!!popup.is_close" class="close" @click="closePopup">
                        <svg width="14" height="14" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11 1L1 11" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M1 1L11 11" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </span>
                    <p>{{popup.text}}</p>
                    <a
                        v-if="!!popup.show_button && popup.button.length > 0"
                        v-for="(item, index) in JSON.parse(popup.button)"
                        @click="onButtonClick"
                        :href="item.link"
                        :class="{'outline': index == 1}"
                        :key="index">
                        {{item.text}}
                    </a>
                </div>

                <div
                    v-if="popup.position == 'center'"
                    class="modal"
                    :class="{'modal--html': popup.video}">
                    <span v-if="!!popup.is_close" class="close" @click="closePopup">
                        <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11 1L1 11" :stroke="popup.image ? 'white' : '#342F37'" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M1 1L11 11" :stroke="popup.image ? 'white' : '#342F37'" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </span>
                    <img :src="popup.image" />
                    <div class="modal__body">
                        <h2>{{popup.title}}</h2>
                        <p>{{popup.text}}</p>
                    </div>
                    <div class="modal__footer">
                        <a
                            v-if="!!popup.show_button && popup.button.length > 0"
                            v-for="(item, index) in JSON.parse(popup.button)"
                            @click="onButtonClick"
                            :href="item.link"
                            :class="{'outline': index == 1}"
                            :key="index">
                            {{item.text}}
                        </a>
                    </div>
                    <div v-if="popup.video" class="modal__footer">
                        <iframe :src="popup.video" frameborder="0" width="100%" height="315"></iframe>
                    </div>
                </div>

                <div
                    v-if="popup.position == 'top' || popup.position == 'bottom'"
                    class="alert">
                    <p v-html="popup.text"></p>
                    <a
                        v-if="!!popup.show_button && popup.button.length > 0"
                        v-for="(item, index) in JSON.parse(popup.button)"
                        @click="onButtonClick"
                        :href="item.link"
                        :class="{'outline': index == 1}"
                        :key="index">
                        {{item.text}}
                    </a>
                </div>
            </div>
        </div>
   `,
   data () {
      return {
          apiURL: 'https://api.u2035test.ru/popups',
          positionMap: {
             'bottom': 'cb',
             'left': 'cc',
             'right': 'cc',
             'center': 'cc',
             'right_bottom': 'tr',
             'right_top': 'br',
             'top': 'ct',
          },
          // popup: {
          //     visible: true,
          //     type: 'modal',
          //     variant: 'white',
          //     position: 'cc',
          //     html: true,
          //
          //     resolution: '' //desktop, mobile,
          //
          //     // image: 'https://helpx.adobe.com/content/dam/help/en/photoshop/using/convert-color-image-black-white/jcr_content/main-pars/before_and_after/image-before/Landscape-Color.jpg'
          // },
          popupList: []
      }
   },
   created () {
       console.log('popup init');
       this.loadPopupConfig()
   },
   computed: {
       getPopupBlackout () {
           return this.popupList.filter(popup => popup.blackout == 1).length > 0
       }
   },
   methods: {
       axios (type, url, query) {
           return new Promise((resolve, reject) => {
               let _query = new URLSearchParams(query)

               fetch(`${this.apiURL + url}?${_query}`, {
                   method: type
               })
                    .then(response => resolve(response.json()))
                    .catch(error => reject(error))
           })
       },
       closePopup () {
           console.log('popup closed');
           this.popup.visible = false;
       },
       onButtonClick () {
           console.log('button click');
       },
       loadPopupConfig () {
           this.axios('POST', '/api/v1/popups', {
               url: 'localhost',
               screen: 'desktop',
               unti_id: 141,
           }).then(response => {
               // response.payload
               let _response = {
                    "result": {
                        "successful": true,
                        "code": 200
                    },
                    "payload": [
                        {
                            "id": "01000000-a778-268e-3417-589b7a7a49a3",
                            "title": "One",
                            "text": "One",
                            "url": null,
                            "image": "https://deploy.2035.university/static/1432a0e6/images/svgs/logo.svg",
                            "video": null,
                            "button": "[{\"text\":\"СОгласен\",\"link\":\"https://konga.u2035test.ru/\"}]",
                            "show_button": 1,
                            "position": "center",
                            "screen": "desktop",
                            "style": "white",
                            "blackout": 1,
                            "is_close": 1,
                            "questions": [
                                {
                                    "id": "d2b5c24b-4d1b-11ec-91c5-0242ac120004",
                                    "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
                                    "title": "Вопрос 1",
                                    "type": "text"
                                }
                            ]
                        }
                    ]
                }
                this.popupList = _response.payload
           }).catch(error => {
               console.error('POST ', '/api/v1/popups: ', error)
           })

           console.log('load popup data');
       }
   }
});

new Vue({
    el: '#popups',
})
