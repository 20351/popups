## Развернуть:
- копируем и правим `cp .env.example .env`
- копируем и правим `cp .env.example .env.test` для тестовой базы
- создаем схему `CREATE SCHEMA popups DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;`
- `make up`
- запускаем из контейнера `php console.php recreate/dev`

## Для запуска тестов:
- запускаем из контейнера `php console.php recreate/test`
- используем `test/phpunit.xml`

## Консольные команды:
- kafka/read topic