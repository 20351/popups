FROM debian:buster as base

ENTRYPOINT ["/usr/local/bin/docker-unit.sh"]
CMD ["/usr/lib/unit/sbin/unitd", "--no-daemon", "--control", "unix:/var/run/control.unit.sock", "--pid", "/var/run/unit.pid", "--log", "/var/log/unit.log", "--modules", "/usr/lib/unit/modules", "--state", "/usr/lib/unit/state/", "--tmp", "/var/tmp/"]
WORKDIR "/www"
EXPOSE  "8320" "8321" "8322"
STOPSIGNAL SIGTERM

ENV APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE DontWarn
ENV DEBIAN_FRONTEND noninteractive

ENV PHP_VERSION         8.0
ENV PHP_KAFKA_VERSION   5.0.0
ENV UNIT_GIT_VERSION    1.20.0-1

RUN apt-get update \
    && apt-get install -y --no-install-recommends lsb-release ca-certificates curl gnupg gnupg2 gnupg1\
    && curl -s --output /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg \
    && sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list' \
    && apt-get update \
    && apt-get install -y -q --no-install-recommends \
                    git \
                    vim \
                    nano \
                    less \
                    mc \
                    wget \
                    zip \
                    unzip \
                    gcc \
                    build-essential \
                    librdkafka-dev \
                    php-pear \
                    php${PHP_VERSION} \
                    php${PHP_VERSION}-dev \
                    php${PHP_VERSION}-cli \
                    php${PHP_VERSION}-curl \
                    php${PHP_VERSION}-mysql \
                    php${PHP_VERSION}-mysqli \
                    php${PHP_VERSION}-fpm \
                    php${PHP_VERSION}-embed \
                    php${PHP_VERSION}-cgi \
                    php${PHP_VERSION}-common \
                    php${PHP_VERSION}-mbstring \
                    php${PHP_VERSION}-bcmath \
                    php${PHP_VERSION}-xml \
                    php${PHP_VERSION}-intl \
                    php${PHP_VERSION}-gd \
    && ln -sf /dev/stdout /var/log/unit.log \
    && pecl install rdkafka-${PHP_KAFKA_VERSION} \
                && echo "extension=rdkafka.so" > /etc/php/${PHP_VERSION}/cli/conf.d/20-rdkafka.ini     \
                && echo "extension=rdkafka.so" > /etc/php/${PHP_VERSION}/embed/conf.d/20-rdkafka.ini   \
                && echo "extension=rdkafka.so" > /etc/php/${PHP_VERSION}/mods-available/20-rdkafka.ini \
    && cd /opt \
    && wget -O unit.tar.gz https://github.com/nginx/unit/archive/${UNIT_GIT_VERSION}.tar.gz \
    && tar -C ./ -xvf unit.tar.gz --strip-components 1  \
    && ./configure --modules=modules \
    && ./configure php --module=php  \
    && DESTDIR=/usr/lib/unit/  make \
    && DESTDIR=/usr/lib/unit/ make install \
    && rm -Rf /opt/* \
    && apt-get purge -y \
                    autoconf \
                    automake \
                    autopoint \
                    autotools-dev \
                    bsdmainutils \
                    build-essential \
                    cpp debhelper \
                    dh-autoreconf \
                    dh-strip-nondeterminism \
                    dpkg-dev \
                    fakeroot \
                    g++ \
                    g++-7 \
                    gcc \
                    gettext \
                    gettext-base \
                    groff-base \
                    intltool-debian \
                    libalgorithm-diff-perl \
                    libalgorithm-diff-xs-perl \
                    libalgorithm-merge-perl \
                    libarchive-zip-perl \
                    libatomic1 \
                    libc6-dev \
                    libcc1-0 \
                    libc-dev-bin \
                    libcilkrts5 \
                    libcroco3 \
                    libdpkg-perl \
                    libfakeroot \
                    libfile-fcntllock-perl \
                    libfile-stripnondeterminism-perl \
                    libgomp1 \
                    libitm1 \
                    liblocale-gettext-perl \
                    liblsan0 \
                    libltdl-dev \
                    libmail-sendmail-perl \
                    libmpc3 \
                    libmpx2 \
                    libpcre2-16-0 \
                    libpcre2-32-0 \
                    libpcre2-dev \
                    libpipeline1 \
                    libquadmath0 \
                    libsigsegv2 \
                    libssl-dev \
                    libsys-hostname-long-perl \
                    libtimedate-perl \
                    libtool libtsan0 \
                    libubsan0 \
                    linux-libc-dev \
                    m4 make \
                    man-db \
                    manpages \
                    manpages-dev \
                    netbase \
                    patch perl \
                    php-dev \
                    php-pear \
                    pkg-php-tools \
                    po-debconf \
                    shtool \
                    binutils \
                    binutils-x86-64-linux-gnu \
                    binutils-common \
                    cpp-7 gcc-7 \
                    gcc-7-base \
                    libarchive-cpio-perl \
                    libasan4 \
                    libbinutils \
                    libgcc-7-dev \
                    libgdbm-compat4 \
                    libisl19 \
                    libmpfr6 \
                    libpcre2-posix2 \
                    libperl5.26 \
                    libstdc++-7-dev \
    && apt-get install -y git procps \
    && apt autoremove -y \
    && apt clean -y \
    && apt autoclean -y \
    && apt purge -y --auto-remove

# Add composer
ENV COMPOSER_ALLOW_SUPERUSER    1
ENV COMPOSER_VERSION            2.0.14

RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
    && php /tmp/composer-setup.php --no-ansi --version="${COMPOSER_VERSION}" --install-dir=/usr/local/bin --filename=composer \
    && composer -V

ARG NO_CACHE
COPY composer.json composer.lock ./
ARG CODE_2035_TOKEN

RUN if [ "$CODE_2035_TOKEN" != "" ] ; then \
        echo "Token is provided!" \
        && composer config -g gitlab-domains code.2035.university \
        && composer config -g gitlab-token.code.2035.university $CODE_2035_TOKEN \
        && composer install \
        ; \
    else \
        echo "Token not provided. Try try CI mode!" \
        && composer config -g gitlab-domains code.2035.university \
        && curl -o  ~/.config/composer/auth.json http://security.2bond.cloud/code_2035_token_composer \
        && composer install \
        && rm  ~/.config/composer/auth.json \
        ; \
    fi

COPY docker/docker-unit.sh ./docker/docker-hooks.sh /usr/local/bin/
COPY docker/docker-entrypoint.sh docker/docker-unit-config.json /docker-entrypoint.d/
COPY console.php ./

ADD --chown=nobody:nogroup  web ./web
ADD --chown=nobody:nogroup  test ./test
ADD --chown=nobody:nogroup  messages ./messages
ADD --chown=nobody:nogroup  migrations ./migrations
ADD --chown=nobody:nogroup  src ./src
ADD --chown=nobody:nogroup  runtime ./runtime

FROM base as console
CMD php ./console.php ${CONSOLE} *${HOSTNAME}

FROM base as xdebug
RUN apt-get update \
    && apt-get install php${PHP_VERSION}-xdebug \
    && echo "xdebug.mode = coverage,develop"     >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.start_with_request = yes"          >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.discover_client_host = 1"          >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.client_host = docker.host.name"          >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.client_port = 9003"                >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.var_display_max_depth = -1"        >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.var_display_max_children = -1"     >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.var_display_max_data = -1"         >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.max_nesting_level = 500"           >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.remote_client = 127.0.0.1"         >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini
