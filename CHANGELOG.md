# Changelog
Все существенные изменения будут отражены в это файле.

Формат файла основан на сайте [Keep a Changelog](https://keepachangelog.com/ru/1.0.0/).
Проект придерживается [семантического версионирования](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2021-11-30
### Added
- Базовая структура приложения [POPUPS-1](https://jira.2035.university/browse/POPUPS-1)
