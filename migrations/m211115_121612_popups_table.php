<?php

use yii\db\Migration;

/**
 */
class m211115_121612_popups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%popup}}', [
            'id' => $this->string(36)->notNull(),
            'title' => $this->string(1000),
            'text' => $this->text(),
            'url' => $this->string(255),
            'image' => $this->string(255),
            'video' => $this->string(255),
            'button' => $this->text(),
            'show_button'=>$this->boolean()->defaultValue(0),
            'position' => 'ENUM("top", "bottom", "center", "right_top", "right_left") default "top"',
            'screen' => 'ENUM("mobile", "desktop") default "desktop"',
            'style' => 'ENUM("white", "blue","error") default "white"',
            'blackout' => $this->boolean()->defaultValue(0),
            'is_close' => $this->boolean()->defaultValue(1),
            'show_one_user' => $this->integer(1)->defaultValue(0),
            'show_guest' => $this->boolean()->defaultValue(0),
            'show_without_reaction' => $this->integer(1)->defaultValue(0),
            'number_launch' => $this->integer(1)->defaultValue(0),
            'start_dt' => $this->dateTime(3),
            'end_dt' => $this->dateTime(3),
            'active' => $this->boolean()->defaultValue(0),
            'is_deleted' => $this->boolean()->defaultValue(0),
            'created_at' => $this->dateTime(3),
            'updated_at' => $this->dateTime(3),
            'PRIMARY KEY (id)',
        ],
               'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci'
        );

        $this->createIndex('active', '{{%popup}}', 'active');
        $this->createIndex('is_deleted', '{{%popup}}', 'is_deleted');

        $this->createTable('{{%popup_condition}}', [
            'popup_id' => $this->string(36),
            'pattern' => $this->string(255),
            'created_at' => $this->dateTime(3)
        ],
                           'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci'
        );

        $this->createIndex('popup_id', '{{%popup_condition}}', 'popup_id');
        $this->addForeignKey('popup_id_popup_condition', '{{%popup_condition}}', 'popup_id','popup','id','CASCADE');

        $this->createTable('{{%popup_tag}}', [
            'popup_id' => $this->string(36),
            'tag_id' => $this->string(36),
            'tag_guid' => $this->string(255),
            'created_at' => $this->dateTime(3)
        ],
                           'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci'
        );

        $this->createIndex('tag_id', '{{%popup_tag}}', 'tag_id');
        $this->createIndex('popup_id', '{{%popup_tag}}', 'popup_id');
        $this->addForeignKey('popup_id_popup_tag', '{{%popup_tag}}', 'popup_id','popup','id','CASCADE');

        $this->createTable('{{%popup_context}}', [
            'popup_id' => $this->string(36),
            'context_id' => $this->string(36),
            'context_guid' => $this->string(255),
            'created_at' => $this->dateTime(3)
        ],
                           'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci'
        );

        $this->createIndex('context_id', '{{%popup_context}}', 'context_id');
        $this->createIndex('popup_id', '{{%popup_context}}', 'popup_id');
        $this->addForeignKey('popup_id_popup_context', '{{%popup_context}}', 'popup_id','popup','id','CASCADE');

        $this->createTable('{{%popup_updated_log}}', [
            'popup_id' => $this->string(36),
            'unti_id' => $this->integer(),
            'created_at' => $this->dateTime(3)
        ],
                           'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci'
        );

        $this->createIndex('unti_id', '{{%popup_updated_log}}', 'unti_id');
        $this->createIndex('popup_id', '{{%popup_updated_log}}', 'popup_id');
        $this->addForeignKey('popup_id_popup_updated_log', '{{%popup_updated_log}}', 'popup_id','popup','id','CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropForeignKey('popup_id_popup_updated_log', '{{%popup_updated_log}}');
        $this->dropTable('{{%popup_updated_log}}');

        $this->dropForeignKey('popup_id_popup_context', '{{%popup_context}}');
        $this->dropTable('{{%popup_context}}');
        $this->dropForeignKey('popup_id_popup_tag', '{{%popup_tag}}');
        $this->dropTable('{{%popup_tag}}');
        $this->dropForeignKey('popup_id_popup_condition', '{{%popup_condition}}');
        $this->dropTable('{{%popup_condition}}');

        $this->dropIndex('active', '{{%popup}}');
        $this->dropIndex('is_deleted', '{{%popup}}');
        $this->dropTable('{{%popup}}');
    }
}