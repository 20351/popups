<?php

use yii\db\Migration;

/**
 */
class m211115_121512_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'unti_id' => $this->integer(),
            'email' => $this->string(255),
            'lastname' => $this->string(255),
            'firstname' => $this->string(255),
            'middlename' => $this->string(255),
            'image' => $this->string(255),
            'hash' => $this->text(),
            'is_deleted' => $this->integer(1)->defaultValue(0),
            'access_token' => $this->string(255),
            'created_at' => $this->dateTime(3),
            'updated_at' => $this->dateTime(3),
            'PRIMARY KEY (unti_id)',
        ],
               'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci'
        );

        $this->createIndex('access_token', '{{%user}}', 'access_token');
        $this->createIndex('is_deleted', '{{%user}}', 'is_deleted');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('access_token', '{{%user}}');
        $this->dropIndex('is_deleted', '{{%user}}');
        $this->dropTable('{{%user}}');
    }
}