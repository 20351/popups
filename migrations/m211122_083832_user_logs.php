<?php

use yii\db\Migration;

/**
 */
class m211122_083832_user_logs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            '{{%popup_question}}',
            [
                'id' => $this->string(36)->notNull(),
                'popup_id' => $this->string(36),
                'title' => $this->string(255),
                'type' => 'ENUM("text", "rating") default "text"',
                'created_at' => $this->dateTime(3),
                'updated_at' => $this->dateTime(3),
                'PRIMARY KEY (id)',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci'
        );
        $this->createIndex('popup_id', '{{%popup_question}}', 'popup_id');
        $this->addForeignKey('popup_id_popup_question', '{{%popup_question}}', 'popup_id', 'popup', 'id', 'CASCADE');

        $this->createTable(
            '{{%user_answer}}',
            [
                'id' => $this->string(36)->notNull(),
                'question_id' => $this->string(36),
                'unti_id' => $this->integer(),
                'guest_id' => $this->string(255),
                'answer' => $this->text(),
                'created_at' => $this->dateTime(3),
                'updated_at' => $this->dateTime(3),
                'PRIMARY KEY (id)',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci'
        );
        $this->createIndex('question_id', '{{%user_answer}}', 'question_id');
        $this->addForeignKey(
            'question_id_user_answer',
            '{{%user_answer}}',
            'question_id',
            'popup_question',
            'id',
            'CASCADE'
        );

        $this->createTable(
            '{{%user_tracker_log}}',
            [
                'id' => $this->string(36)->notNull(),
                'popup_id' => $this->string(36),
                'unti_id' => $this->integer(),
                'guest_id' => $this->string(255),
                'url' => $this->text(),
                'context_id' => $this->string(36),
                'show' => $this->boolean()->defaultValue(0),
                'reaction' => $this->boolean()->defaultValue(0),
                'skip' => $this->boolean()->defaultValue(0),
                'created_at' => $this->dateTime(3),
                'updated_at' => $this->dateTime(3),
                'PRIMARY KEY (id)',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci'
        );

        $this->createIndex('popup_id', '{{%user_tracker_log}}', 'popup_id');
        $this->createIndex('unti_id', '{{%user_tracker_log}}', 'unti_id');
        $this->createIndex('guest_id', '{{%user_tracker_log}}', 'guest_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('popup_id', '{{%user_tracker_log}}');
        $this->dropIndex('unti_id', '{{%user_tracker_log}}');
        $this->dropIndex('guest_id', '{{%user_tracker_log}}');
        $this->dropTable('{{%user_tracker_log}}');

        $this->dropForeignKey('question_id_user_answer', '{{%user_answer}}');
        $this->dropIndex('question_id', '{{%user_answer}}');
        $this->dropTable('{{%user_answer}}');

        $this->dropForeignKey('popup_id_popup_question', '{{%popup_question}}');
        $this->dropIndex('popup_id', '{{%popup_question}}');
        $this->dropTable('{{%popup_question}}');
    }
}