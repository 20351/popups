<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%casbin_rule}}`.
 */
class m211004_141512_create_casbin_rule_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            '{{%casbin_rule}}',
            [
                'id' => $this->primaryKey(),
                'ptype' => $this->string(255),
                'v0' => $this->string(255),
                'v1' => $this->string(255),
                'v2' => $this->string(255),
                'v3' => $this->string(255),
                'v4' => $this->string(255),
                'v5' => $this->string(255),
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci'
        );
        $this->createIndex('index2', '{{%casbin_rule}}', ['v0', 'v1', 'v2']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%casbin_rule}}');
    }
}