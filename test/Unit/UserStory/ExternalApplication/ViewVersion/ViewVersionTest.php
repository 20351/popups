<?php

declare(strict_types=1);

namespace Unit\UserStory\ExternalApplication\ViewVersion;

use Popups\UserStory\ExternalApplication\ViewVersion\ViewVersion;
use PHPUnit\Framework\TestCase;

class ViewVersionTest extends TestCase
{
    public function testSuccessful()
    {
        $result = (new ViewVersion())->response();
        $this->assertTrue($result->isSuccessful());
    }
}