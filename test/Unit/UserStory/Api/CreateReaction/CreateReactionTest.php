<?php

declare(strict_types=1);

namespace Popups\Tests\Unit\UserStory\Api\CreateReaction;

use Infrastructure\Application\Database\Truncate;
use PHPUnit\Framework\TestCase;
use Popups\Tests\Infrastructure\Application\TestApplication;
use Popups\Tests\Infrastructure\Seed\ReactionSeed;
use Popups\UserStory\Api\CreateReaction\CreateReaction;
use Popups\UserStory\Api\CreateReaction\Validation\ValidatedCreateReactionCommand;
use yii\helpers\Json;

class CreateReactionTest extends TestCase
{

    public function testSuccessful()
    {
        $result = (
        new CreateReaction(
            new ValidatedCreateReactionCommand(
                $this->id(),
                $this->popup()
            )
        )
        )
            ->response();

        self::assertTrue($result->isSuccessful());
    }

    public function testSuccessfulContext()
    {
        $result = (
        new CreateReaction(
            new ValidatedCreateReactionCommand(
                $this->id(),
                $this->popup2()
            )
        )
        )
            ->response();

        self::assertTrue($result->isSuccessful());
    }

    public function testSuccessfulTags()
    {
        $result = (
        new CreateReaction(
            new ValidatedCreateReactionCommand(
                $this->id(),
                $this->popup3()
            )
        )
        )
            ->response();

        self::assertTrue($result->isSuccessful());
    }

    public function testFailed()
    {
        $result = (
        new CreateReaction(
            new ValidatedCreateReactionCommand(
                'we',
                $this->popup()
            )
        )
        )
            ->response();
        self::assertFalse($result->isSuccessful());
    }

    private function id(): string
    {
        return '01000000-a778-268e-3417-589b7a7a49d2';
    }

    private function popup(): array
    {
        return [
            'show' => 0,
            'close' => 1,
            'reacton' => 1,
            'answers' => [['question_id' => '01000000-a778-268e-3417-589b7a7a49d3', 'answer' => 'text']],
        ];
    }
    private function popup2(): array
    {
        return [
            'show' => 1
        ];
    }

    private function popup3(): array
    {
        return [
            'show' => 0,
            'close' =>0,
            'reacton' => 1,
        ];
    }


    protected function setUp(): void
    {
        (new TestApplication())->application();
        (new Truncate('popup', 'popup_context', 'popup_tag', 'popup_condition', 'popup_question', 'user','user_answer', 'user_tracker_log', 'user'))->run();
        (new ReactionSeed())->run();
    }
}
