<?php

declare(strict_types=1);

namespace Popups\Tests\Unit\UserStory\Api\CreateReaction\Validation;

use PHPUnit\Framework\TestCase;
use Popups\Tests\Infrastructure\Application\TestApplication;
use Popups\UserStory\Api\CreateReaction\Validation\ValidatedCreateReactionCommand;
use yii\helpers\Json;

class ValidatedCreateReactionCommandTest extends TestCase
{
    public function testSuccessful()
    {
        $result =
            (new ValidatedCreateReactionCommand(
                $this->id(),
                $this->popup()
            ))
                ->result();
        self::assertTrue($result->isSuccessful());
        self::assertEquals(
            0,
            $result->value()['show']
        );
    }


    public function testWithout()
    {
        $result =
            (new ValidatedCreateReactionCommand(
                $this->id(),
                $this->failed()
            ))
                ->result();

        self::assertFalse($result->isSuccessful());
        self::assertEquals(
            [
                'show' => ['Show must be an integer'],
            ],
            $result->error()
        );
    }

    protected function setUp(): void
    {
        (new TestApplication())->application();
    }

    private function id(): string
    {
        return '01000000-a778-268e-3417-589b7a7a49d3';
    }

    private function popup(): array
    {
        return [
            'show' => 0,
            'close' => 1,
            'reacton' => 1
        ];
    }

    private function failed(): array
    {
        return [
            'show' => 'asdsda',
        ];
    }

}
