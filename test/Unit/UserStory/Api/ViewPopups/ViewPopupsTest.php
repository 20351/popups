<?php

declare(strict_types=1);

namespace Popups\Tests\Unit\UserStory\Api\ViewPopups;

use GuzzleHttp\Client;
use Http\Transport\DefaultHttpTransport;
use Infrastructure\Application\Database\Truncate;
use Infrastructure\Tests\Infrastructure\Validation\SuccessfulValidation;
use PHPUnit\Framework\TestCase;
use Popups\Tests\Infrastructure\Application\TestApplication;
use Popups\Tests\Infrastructure\Http\Transport\FakeUser;
use Popups\Tests\Infrastructure\Http\Transport\FakeUserNotTags;
use Popups\Tests\Infrastructure\Seed\ReactionSeed;
use Popups\UserStory\Api\ViewPopupsList\ViewPopupsList;

class ViewPopupsTest extends TestCase
{

    public function testSuccesfulPopup()
    {
        $result =
            (
            new ViewPopupsList(
                new SuccessfulValidation(
                    [
                        'url' => 'https://pt.u2035test.ru',
                        'guest_id' => 'as'
                    ]
                ),
                new DefaultHttpTransport(new Client(['http_errors' => false]))
            )
            )
                ->response();

        self::assertTrue($result->isSuccessful());
        self::assertCount(1, $result->payload());
    }

    public function testSuccesfulPopupMobile()
    {
        $result =
            (
            new ViewPopupsList(
                new SuccessfulValidation(
                    [
                        'url' => 'https://pt.u2035test.ru/',
                        'screen' => 'mobile',
                        'guest_id' => 'as'
                    ]
                ),
                new DefaultHttpTransport(new Client(['http_errors' => false]))
            )
            )
                ->response();

        self::assertTrue($result->isSuccessful());
        self::assertCount(1, $result->payload());
    }

    public function testSuccesfulPopupContext()
    {
        $result =
            (
            new ViewPopupsList(
                new SuccessfulValidation(
                    [
                        'url' => 'https://pt.u2035test.ru',
                        'context' => '01000000-a778-268e-3417-589b7a7a49d3',
                        'guest_id' => 'as'
                    ]
                ),
                new DefaultHttpTransport(new Client(['http_errors' => false]))
            )
            )
                ->response();
        self::assertTrue($result->isSuccessful());
        self::assertEquals($this->id1(), $result->payload()[0]['id']);
    }

    public function testSuccesfulPopup3()
    {
        $result =
            (new ViewPopupsList(
                new SuccessfulValidation(
                    [
                        'url' => 'https://pt.u2035test.ru',
                        'screen' => 'mobile',
                        'guest_id' => 'as'
                    ]
                ),
                new DefaultHttpTransport(new Client(['http_errors' => false]))
            )
            )
                ->response();

        self::assertTrue($result->isSuccessful());
        self::assertEquals($this->id2(), $result->payload()[0]['id']);
    }
    public function testSuccesfulPopupUrl2()
    {
        $result =
            (new ViewPopupsList(
                new SuccessfulValidation(
                    [
                        'url' => 'https://pt.u2035test.ru/?context=1',
                        'screen' => 'mobile',
                        'guest_id' => 'as'
                    ]
                ),
                new DefaultHttpTransport(new Client(['http_errors' => false]))
            )
            )
                ->response();

        self::assertTrue($result->isSuccessful());
        self::assertEquals($this->id2(), $result->payload()[0]['id']);
    }

    public function testSuccesfulPopupTags()
    {
        $result =
            (new ViewPopupsList(
                new SuccessfulValidation(
                    [
                        'url' => 'https://pt.u2035test.ru/project/index',
                        'unti_id' => 141
                    ]
                ),
                new FakeUser()
            )
            )
                ->response();

        self::assertTrue($result->isSuccessful());
        self::assertEquals($this->id3(), $result->payload()[0]['id']);
    }
    public function testSuccesfulPopupNotTags()
    {
        $result =
            (new ViewPopupsList(
                new SuccessfulValidation(
                    [
                        'url' => 'https://pt.u2035test.ru/project/index',
                        'unti_id' => 141
                    ]
                ),
                new FakeUserNotTags()
            )
            )
                ->response();

        self::assertTrue($result->isSuccessful());
        self::assertCount(0, $result->payload());
    }

    public function testDataWithAll()
    {
        $result =
            (new ViewPopupsList(
                new SuccessfulValidation(
                    [
                        'url' => 'https://pt.u2035test.ru/project/index',
                        'context' => '01000000-a778-268e-3417-589b7a7a49d3',
                        'unti_id' => 141,
                        'screen' => 'desktop',
                    ]
                ),
                new FakeUser()
            )
            )
                ->response();
        self::assertTrue($result->isSuccessful());
        self::assertCount(0, $result->payload());
    }


    public function testFailedDataWithUrl()
    {
        $result =
            (new ViewPopupsList(
                new SuccessfulValidation(
                    [
                        'context' => '01000000-a778-268e-3417-589b7a7a49d3',
                        'screen' => 'mobile',
                        'guest_id' => 'as'
                    ]
                ),
                new DefaultHttpTransport(new Client(['http_errors' => false]))
            )
            )
                ->response();

        self::assertFalse($result->isSuccessful());
        self::assertEquals(400, $result->code());
    }

    public function id1(): string
    {
        return '01000000-a778-268e-3417-589b7a7a49a3';
    }

    public function id2(): string
    {
        return '01000000-a778-268e-3417-589b7a7a49a4';
    }

    public function id3(): string
    {
        return '01000000-a778-268e-3417-589b7a7a49a5';
    }

    protected function setUp(): void
    {
        (new TestApplication())->application();
        (new Truncate(
            'popup',
            'popup_context',
            'popup_tag',
            'popup_condition',
            'popup_question',
            'user',
            'user_answer',
            'user_tracker_log',
            'user'
        ))->run();
        (new ReactionSeed())->run();
    }
}
