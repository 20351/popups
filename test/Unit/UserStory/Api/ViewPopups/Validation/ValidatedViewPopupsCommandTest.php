<?php

declare(strict_types=1);

namespace Popups\Tests\Unit\UserStory\Api\ViewPopups\Validation;

use PHPUnit\Framework\TestCase;
use Popups\Tests\Infrastructure\Application\TestApplication;
use Popups\UserStory\Api\ViewPopupsList\Validation\ValidatedViewPopupsListCommand;


class ValidatedViewPopupsCommandTest extends TestCase
{
    /**
     * @dataProvider successfulParams
     */
    public function testSuccessful(?array $values)
    {
        $result = (new ValidatedViewPopupsListCommand($values))->result();

        self::assertTrue($result->isSuccessful());
    }

    public function successfulParams(): array
    {
        return [
            [['url'=>'http']],
            [['unti_id' => 141,'url'=>'http']],
            [['unti_id' => 141,'url'=>'http','context'=>'01000000-a778-268e-3417-589b7a7a49d3']],
            [['unti_id' => 141,'url'=>'http','context'=>'01000000-a778-268e-3417-589b7a7a49d3','screen'=>'mobile']],
            [['url'=>'http','context'=>'01000000-a778-268e-3417-589b7a7a49d3','screen'=>'mobile','guest_id'=>'as']],
        ];
    }

    /**
     * @dataProvider failedParams
     */
    public function testFailed(?array $values, array $expectedError)
    {
        $result = (new ValidatedViewPopupsListCommand($values))->result();

        self::assertFalse($result->isSuccessful());
        self::assertEquals($expectedError, $result->error());
    }

    public function failedParams(): array
    {
        return [
            [
                [],
                [
                    'url' => ['Url is required'],
                ]
            ],
            [
                ['unti_id' => '141','url'=>'http','context'=>'as'],
                [
                    'unti_id' => ['Unti Id must be an integer'],
                ]
            ],
            [
                ['unti_id' => 141,'url'=>'http','context'=>'as'],
                [
                    'context' => ['Context is not UUID'],
                ]
            ],

        ];
    }

    protected function setUp(): void
    {
        (new TestApplication())->application();
    }
}
