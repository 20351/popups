<?php

declare(strict_types=1);

namespace Popups\Tests\Unit\UserStory\Admin\DeletePopup;

use Infrastructure\Application\Database\Truncate;
use Infrastructure\Tests\Infrastructure\Validation\SuccessfulValidation;
use PHPUnit\Framework\TestCase;
use Popups\Tests\Infrastructure\Application\TestApplication;
use Popups\Tests\Infrastructure\Seed\TestSeed;
use Popups\UserStory\Admin\DeletePopup\DeletePopup;
use Popups\UserStory\Admin\ViewPopup\ViewPopup;

class DeletePopupTest extends TestCase
{
    public function testSuccessful(): void
    {
        $result = (
        new DeletePopup(new SuccessfulValidation(['id' => '01000000-a778-268e-3417-589b7a7a49d3']))
        )
            ->response();

        self::assertTrue($result->isSuccessful());
        $result = (
        new ViewPopup(new SuccessfulValidation(['id' => '01000000-a778-268e-3417-589b7a7a49d3']))
        )
            ->response();

        self::assertTrue($result->isSuccessful());
        self::assertEquals(
            1,
            $result->payload()['is_deleted']
        );
    }

    public function testSuccessfulToo(): void
    {
        $result = (
        new DeletePopup(new SuccessfulValidation(['id' => '01000000-a778-268e-3417-589b7a7a49a3']))
        )
            ->response();

        self::assertTrue($result->isSuccessful());
        $result = (
        new ViewPopup(new SuccessfulValidation(['id' => '01000000-a778-268e-3417-589b7a7a49a3']))
        )
            ->response();

        self::assertTrue($result->isSuccessful());
        self::assertEquals(
            1,
            $result->payload()['is_deleted']
        );
    }

    public function testNonExistentObject(): void
    {
        $result = (new DeletePopup(new SuccessfulValidation(['id' => 'not-existent-id'])))
            ->response();

        self::assertEquals(404, $result->code());
    }

    protected function setUp(): void
    {
        (new TestApplication())->application();
        (new Truncate('popup', 'popup_context', 'popup_tag', 'user'))->run();
        (new TestSeed())->run();
    }
}
