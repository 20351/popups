<?php

declare(strict_types=1);

namespace Popups\Tests\Unit\UserStory\Admin\UpdatePopup\Validation;

use PHPUnit\Framework\TestCase;
use Popups\Domain\Popup\Position\Top;
use Popups\Domain\Popup\Screen\Mobile;
use Popups\Domain\Popup\Style\White;
use Popups\Tests\Infrastructure\Application\TestApplication;
use Popups\UserStory\Admin\UpdatePopup\Validation\ValidatedUpdatePopupCommand;
use yii\helpers\Json;

class ValidatedUpdatePopupCommandTest extends TestCase
{
    public function testSuccessful()
    {
        $result =
            (new ValidatedUpdatePopupCommand(
                $this->popup()
            ))
                ->result();
        self::assertTrue($result->isSuccessful());
        self::assertEquals(
            'Окно 1',
            $result->value()['title']
        );
    }

    public function testFailedString()
    {
        $post =  $this->popup();
        $post['position'] = false;
        $result =
            (new ValidatedUpdatePopupCommand(
                $post
            ))
                ->result();

        self::assertFalse($result->isSuccessful());
        self::assertEquals(
            [
                'position' => ['Position is not string'],
            ],
            $result->error()
        );
    }
    public function testFailedContext()
    {
        $post =  $this->popup();
        $post['contexts'] = false;
        $result =
            (new ValidatedUpdatePopupCommand(
                $post
            ))
                ->result();

        self::assertFalse($result->isSuccessful());
        self::assertEquals(
            [
                'contexts' => ['Contexts is not array'],
            ],
            $result->error()
        );
    }
    public function testWithoutTitle()
    {
        $result =
            (new ValidatedUpdatePopupCommand(
                $this->failed()
            ))
                ->result();

        self::assertFalse($result->isSuccessful());
        self::assertEquals(
            [
                'title' => ['Title is required'],
            ],
            $result->error()
        );
    }

    protected function setUp(): void
    {
        (new TestApplication())->application();
    }

    private function popup(): array
    {
        return [
            'id' => '01000000-a778-268e-3417-589b7a7a49d3',
            'title' => 'Окно 1',
            'text' => 'aaa',
            'image' => 'aaa',
            'video' => 'aaa',
            'show_one_user' => 4,
            'show_guest' => true,
            'show_without_reaction' => 3,
            'number_launch' => 1,
            'start_dt' => null,
            'show_button' => true,
            'blackout' => true,
            'is_close' => true,
            'active' => true,
            'position' => (new Top())->name(),
            'screen' => (new Mobile())->name(),
            'style' => (new White())->name(),
            'contexts'=>['01000000-a778-268e-3417-589b7a7a49d3'],
            'tags'=>['01000000-a778-268e-3417-589b7a7a49d3'],
        ];
    }

    private function failed(): array
    {
        return [
            'id' => '01000000-a778-268e-3417-589b7a7a49d3',
            'text' => 'aaa',
            'show_button' => 1,
            'blackout' => 1,
            'is_close' => 1,
            'active' => 1,
            'position' => (new Top())->name(),
            'screen' => (new Mobile())->name(),
            'style' => (new White())->name()
        ];
    }

}
