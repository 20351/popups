<?php

declare(strict_types=1);

namespace Popups\Tests\Unit\UserStory\Admin\UpdatePopup;

use Infrastructure\Application\Database\Truncate;
use PHPUnit\Framework\TestCase;
use Popups\Domain\Popup\Position\Top;
use Popups\Domain\Popup\Screen\Mobile;
use Popups\Domain\Popup\Style\White;
use Popups\Tests\Infrastructure\Application\TestApplication;
use Popups\Tests\Infrastructure\Seed\TestSeed;
use Popups\UserStory\Admin\UpdatePopup\UpdatePopup;
use Popups\UserStory\Admin\UpdatePopup\Validation\ValidatedUpdatePopupCommand;
use Popups\UserStory\Admin\ViewPopup\Validation\ValidatedViewPopupCommand;
use Popups\UserStory\Admin\ViewPopup\ViewPopup;

class UpdatePopupTest extends TestCase
{


    public function testSuccessful()
    {
        $result = (
        new UpdatePopup(
            $this->id(),
            new ValidatedUpdatePopupCommand(
                $this->popup()
            ),
            141
        )
        )
            ->response();

        self::assertTrue($result->isSuccessful());

        $result = (
        new ViewPopup(
            new ValidatedViewPopupCommand($this->id())
        )
        )
            ->response();
        self::assertTrue($result->isSuccessful());

        self::assertEquals(
            $this->title(),
            $result->payload()['title']
        );
    }

    public function testSuccessfulContext()
    {
        $result = (
        new UpdatePopup(
            $this->id(),
            new ValidatedUpdatePopupCommand(
                $this->popup()
            ),
            141
        )
        )
            ->response();

        self::assertTrue($result->isSuccessful());

        $result = (
        new ViewPopup(
            new ValidatedViewPopupCommand($this->id())
        )
        )
            ->response();
        self::assertTrue($result->isSuccessful());
        self::assertEquals(
            $this->popup()['contexts'][0],
            $result->payload()['contexts'][0]['context_id']
        );
    }

    public function testSuccessfulTags()
    {
        $result = (
        new UpdatePopup(
            $this->id(),
            new ValidatedUpdatePopupCommand(
                $this->popup()
            ),
            141
        )
        )
            ->response();

        self::assertTrue($result->isSuccessful());

        $result = (
        new ViewPopup(
            new ValidatedViewPopupCommand($this->id())
        )
        )
            ->response();
        self::assertTrue($result->isSuccessful());
        self::assertEquals(
            $this->popup()['tags'][0],
            $result->payload()['tags'][0]['tag_id']
        );
    }

    public function testFailed()
    {
        $result = (
        new UpdatePopup(
            $this->id(),
            new ValidatedUpdatePopupCommand(
                $this->failed()
            ),
            141
        )
        )
            ->response();
        self::assertFalse($result->isSuccessful());
    }

    private function title(): string
    {
        return 'Error';
    }

    private function id(): string
    {
        return '01000000-a778-268e-3417-589b7a7a49d3';
    }

    private function popup(): array
    {
        return [
            'title' => 'Error',
            'text' => 'aaa',
            'image' => 'aaa',
            'video' => 'aaa',
            'show_one_user' => 4,
            'show_guest' => true,
            'show_without_reaction' => 3,
            'number_launch' => 1,
            'start_dt' => null,
            'show_button' => true,
            'blackout' => true,
            'is_close' => true,
            'active' => true,
            'position' => (new Top())->name(),
            'screen' => (new Mobile())->name(),
            'style' => (new White())->name(),
            'contexts' => ['01000000-a778-268e-3417-589b7a7a49d3'],
            'tags' => ['01000000-a778-268e-3417-589b7a7a49d3'],
            'conditions' => ['https://2035.university'],
            'questions' => [['title'=>'https://2035.university','type'=>'text']],
        ];
    }

    private function failed(): array
    {
        return [
            'text' => 'aaa',
            'image' => 'aaa',
            'video' => 'aaa',
            'show_one_user' => 4,
            'show_guest' => true,
            'show_without_reaction' => 3,
            'number_launch' => 1,
            'start_dt' => null,
            'show_button' => true,
            'blackout' => true,
            'is_close' => true,
            'active' => true,
            'position' => (new Top())->name(),
            'screen' => (new Mobile())->name(),
            'style' => (new White())->name(),
            'contexts' => ['01000000-a778-268e-3417-589b7a7a49d3'],
            'tags' => ['01000000-a778-268e-3417-589b7a7a49d3'],
            'conditions' => ['https://2035.university'],
            'questions' => [['title'=>'https://2035.university','type'=>'text']],
        ];
    }


    protected function setUp(): void
    {
        (new TestApplication())->application();
        (new Truncate('popup', 'popup_context', 'popup_tag', 'popup_condition', 'popup_question', 'user'))->run();
        (new TestSeed())->run();
    }
}
