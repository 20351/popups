<?php

declare(strict_types=1);

namespace Popups\Tests\Unit\UserStory\Admin\ViewPopups\Validation;

use PHPUnit\Framework\TestCase;
use Popups\Tests\Infrastructure\Application\TestApplication;
use Popups\UserStory\Admin\ViewPopupsList\Validation\ValidatedViewPopupsListCommand;


class ValidatedViewPopupsCommandTest extends TestCase
{
    /**
     * @dataProvider successfulParams
     */
    public function testSuccessful(?array $values)
    {
        $result = (new ValidatedViewPopupsListCommand($values))->result();
        self::assertTrue($result->isSuccessful());
    }

    public function successfulParams(): array
    {
        return [
            [[]],
            [['search' => 'привет']],
            [['position' => 'top']],
            [['style' => 'blue']],
            [['screen' => 'desktop']],
            [['context' => ['']]],
            [['tags' => ['']]]
        ];
    }

    /**
     * @dataProvider failedParams
     */
    public function testFailed(?array $values, array $expectedError)
    {
        $result = (new ValidatedViewPopupsListCommand($values))->result();

        self::assertFalse($result->isSuccessful());
        self::assertEquals($expectedError, $result->error());
    }

    public function failedParams(): array
    {
        return [
            [
                ['search' => false],
                [
                    'search' => ['Search is not string'],
                ]
            ]
        ];
    }

    protected function setUp(): void
    {
        (new TestApplication())->application();
    }
}
