<?php

declare(strict_types=1);

namespace Popups\Tests\Unit\UserStory\Admin\ViewPopups;

use Infrastructure\Application\Database\Truncate;
use Infrastructure\Tests\Infrastructure\Validation\FailedValidation;
use Infrastructure\Tests\Infrastructure\Validation\SuccessfulValidation;
use PHPUnit\Framework\TestCase;
use Popups\Models\User;
use Popups\Tests\Infrastructure\Application\TestApplication;
use Popups\Tests\Infrastructure\Seed\TestSeed;
use Popups\UserStory\Admin\ViewPopupsList\ViewPopupsList;

class ViewPopupsTest extends TestCase
{

    public function testSuccesfulPopup()
    {
        $result =
            (
            new ViewPopupsList(
                new SuccessfulValidation([
                                             'search' => null,
                                             'position' => null,
                                             'screen' => null,
                                             'style' => null,
                                             'context' => null,
                                             'tags' => null,
                                         ]),
                User::findOne(141)
            )
            )
                ->response();

        self::assertTrue($result->isSuccessful());
        self::assertCount(3, $result->payload());
    }

    public function testSuccesfulPopupSearch()
    {
        $result =
            (
            new ViewPopupsList(
                new SuccessfulValidation([
                                             'search' => 'Окно',
                                             'position' => null,
                                             'screen' => null,
                                             'style' => null,
                                             'context' => null,
                                             'tags' => null,
                                         ]),
                User::findOne(141)
            )
            )
                ->response();
        self::assertTrue($result->isSuccessful());
        self::assertEquals($this->id(), $result->payload()[0]['id']);
    }

    public function testSuccesfulPopupPosition()
    {
        $result =
            (new ViewPopupsList(
                new SuccessfulValidation([
                                             'search' => null,
                                             'position' => 'bottom',
                                             'screen' => null,
                                             'style' => null,
                                             'context' => null,
                                             'tags' => null,
                                         ]),
                User::findOne(141)
            )
            )
                ->response();

        self::assertTrue($result->isSuccessful());
        self::assertEquals($this->id1(), $result->payload()[0]['id']);
    }

    public function testSuccesfulPopupStyle()
    {
        $result =
            (new ViewPopupsList(
                new SuccessfulValidation([
                                             'search' => null,
                                             'position' => null,
                                             'screen' => null,
                                             'style' => 'error',
                                             'context' => null,
                                             'tags' => null,
                                         ]),
                User::findOne(141)
            )
            )
                ->response();

        self::assertTrue($result->isSuccessful());
        self::assertEquals($this->id1(), $result->payload()[0]['id']);
    }

    public function testDataWithQueryAndPositionAndStyle()
    {
        $result =
            (new ViewPopupsList(
                new SuccessfulValidation([
                                             'search' => null,
                                             'position' => 'top',
                                             'screen' => null,
                                             'style' => 'blue',
                                             'context' => null,
                                             'tags' => null,
                                         ]),
                User::findOne(141)
            )
            )
                ->response();
        self::assertTrue($result->isSuccessful());
        self::assertEquals($this->id2(), $result->payload()[0]['id']);
    }

    public function testFailedDataWithQuery()
    {
        $result =
            (new ViewPopupsList(
                new FailedValidation([
                                         'search' => false,
                                         'position' => null,
                                         'screen' => null,
                                         'style' => null,
                                         'context' => null,
                                         'tags' => null,
                                     ]),
                User::findOne(141)
            )
            )
                ->response();

        self::assertFalse($result->isSuccessful());
        self::assertEquals(400, $result->code());
    }

    public function testFailedDataWithUser()
    {
        $result =
            (new ViewPopupsList(
                new FailedValidation([
                                         'search' => null,
                                         'position' => 'as',
                                         'screen' => null,
                                         'style' => null,
                                         'context' => null,
                                         'tags' => null,
                                     ]),
                User::findOne(141)
            )
            )
                ->response();

        self::assertFalse($result->isSuccessful());
        self::assertEquals(400, $result->code());
    }

    public function id(): string
    {
        return '01000000-a778-268e-3417-589b7a7a49d3';
    }

    public function id1(): string
    {
        return '01000000-a778-268e-3417-589b7a7a49dq';
    }

    public function id2(): string
    {
        return '01000000-a778-268e-3417-589b7a7a49a3';
    }

    protected function setUp(): void
    {
        (new TestApplication())->application();
        (new Truncate('popup', 'popup_context', 'popup_tag', 'user'))->run();
        (new TestSeed())->run();
    }
}
