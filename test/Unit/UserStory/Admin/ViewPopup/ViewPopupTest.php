<?php

declare(strict_types=1);

namespace Popups\Tests\Unit\UserStory\Admin\ViewPopup;

use Infrastructure\Application\Database\Truncate;
use Infrastructure\Tests\Infrastructure\Validation\SuccessfulValidation;
use PHPUnit\Framework\TestCase;
use Popups\Tests\Infrastructure\Application\TestApplication;
use Popups\Tests\Infrastructure\Seed\TestSeed;
use Popups\UserStory\Admin\ViewPopup\ViewPopup;

class ViewPopupTest extends TestCase
{

    public function testSuccessfulViewPopup()
    {
        $result = (new ViewPopup(
            new SuccessfulValidation(['id' => '01000000-a778-268e-3417-589b7a7a49d3'])
        ))
            ->response();

        self::assertTrue($result->isSuccessful());

        self::assertEquals(
            'Окно 1',
            $result->payload()['title']
        );
    }

    public function testSuccessfulViewPopupTitle()
    {
        $result = (new ViewPopup(
            new SuccessfulValidation(['id' => '01000000-a778-268e-3417-589b7a7a49dq'])
        ))
            ->response();

        self::assertTrue($result->isSuccessful());

        self::assertEquals(
            'Ошибка',
            $result->payload()['title']
        );
    }

    public function testNonExistentObject()
    {
        $result = (new ViewPopup(
            new SuccessfulValidation(['id' => 'not-existent-id'])
        ))
            ->response();

        self::assertEquals(404, $result->code());
    }

    protected function setUp(): void
    {
        (new TestApplication())->application();
        (new Truncate('popup', 'popup_context', 'popup_tag', 'user'))->run();
        (new TestSeed())->run();
    }
}
