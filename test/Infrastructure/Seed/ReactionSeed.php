<?php

namespace Popups\Tests\Infrastructure\Seed;

use Popups\Domain\Popup\Position\Bottom;
use Popups\Domain\Popup\Position\Top;
use Popups\Domain\Popup\Screen\Desktop;
use Popups\Domain\Popup\Screen\Mobile;
use Popups\Domain\Popup\Style\Blue;
use Popups\Domain\Popup\Style\Error;
use Popups\Domain\Popup\Style\White;
use Popups\Models\Popup;
use Popups\Models\PopupCondition;
use Popups\Models\PopupContext;
use Popups\Models\PopupQuestion;
use Popups\Models\PopupTag;
use Popups\Models\User;
use Popups\Models\UserTrackerLog;
use Ramsey\Uuid\Uuid;

class ReactionSeed
{
    public function run()
    {
        foreach ($this->popups() as $k => $modelRow) {
            $model = new Popup($modelRow);
            $model->save();

            if ($k == 1) {
                $c = new PopupCondition();
                $c->popup_id = $model->id;
                $c->pattern = 'https://pt.u2035test.ru';
                $c->save();
                $c = new PopupContext();
                $c->popup_id = $model->id;
                $c->context_id = '01000000-a778-268e-3417-589b7a7a49d3';
                $c->save();
                $c = new PopupQuestion();
                $c->popup_id = $model->id;
                $c->id = '01000000-a778-268e-3417-589b7a7a49d3';
                $c->title = 'Вопрос';
                $c->type = 'text';
                $c->save();
            }
            if ($k == 2) {
                $c = new PopupCondition();
                $c->popup_id = $model->id;
                $c->pattern = 'https://pt.u2035test.ru';
                $c->save();

            }
            if ($k == 0) {
                $c = new PopupCondition();
                $c->popup_id = $model->id;
                $c->pattern = 'https://pt.u2035test.ru/project/index';
                $c->save();

                $c = new PopupContext();
                $c->popup_id = $model->id;
                $c->context_id = '01000000-a778-268e-3417-589b7a7a49d3';
                $c->save();
            }
            if ($k == 3) {
                $c = new PopupCondition();
                $c->popup_id = $model->id;
                $c->pattern = 'https://pt.u2035test.ru/project/index';
                $c->save();
                $c = new PopupTag();
                $c->popup_id = $model->id;
                $c->tag_id = 'b5a31d6e-5db4-4174-ac65-26d6206cbbfb';
                $c->save();

            }

        }
        $model = new User($this->user());
        $model->save();

        $c = new UserTrackerLog();
        $c->id = '01000000-a778-268e-3417-589b7a7a49d2';
        $c->popup_id ='01000000-a778-268e-3417-589b7a7a49a3';
        $c->unti_id = $model->unti_id;
        $c->save();
    }

    private function user(): array
    {
        return [
            'unti_id' => '141',
            'email' => 'kuteiko@mail.ru',
            'image' => '',
        ];
    }

    private function popups(): array
    {
        return [
            [
                'id' => '01000000-a778-268e-3417-589b7a7a49d3',
                'title' => 'Окно 1',
                'text' => 'aaa',
                'show_button' => 1,
                'blackout' => 1,
                'show_guest' => 1,
                'show_without_reaction' => 1,
                'number_launch' => 1,
                'show_one_user' => 1,
                'is_close' => 1,
                'active' => 1,
                'position' => (new Top())->name(),
                'screen' => (new Mobile())->name(),
                'style' => (new White())->name()
            ],
            [
                'id' => '01000000-a778-268e-3417-589b7a7a49a3',
                'title' => 'Опрос',
                'text' => 'aaa',
                'show_button' => 1,
                'blackout' => 1,
                'is_close' => 1,
                'show_guest' => 1,
                'show_without_reaction' => 1,
                'number_launch' => 1,
                'show_one_user' => 1,
                'active' => 1,
                'position' => (new Top())->name(),
                'screen' => (new Desktop())->name(),
                'style' => (new Blue())->name()
            ],
            [
                'id' => '01000000-a778-268e-3417-589b7a7a49a4',
                'title' => 'Опрос2',
                'text' => 'aaa',
                'show_button' => 1,
                'blackout' => 1,
                'show_guest' => 1,
                'show_without_reaction' => 1,
                'number_launch' => 1,
                'show_one_user' => 1,
                'is_close' => 1,
                'active' => 1,
                'position' => (new Top())->name(),
                'screen' => (new Mobile())->name(),
                'style' => (new Blue())->name()
            ],
            [
                'id' => '01000000-a778-268e-3417-589b7a7a49a5',
                'title' => 'Опрос2',
                'text' => 'aaa',
                'show_button' => 1,
                'blackout' => 1,
                'show_guest' => 1,
                'show_without_reaction' => 1,
                'number_launch' => 1,
                'show_one_user' => 1,
                'is_close' => 1,
                'active' => 1,
                'position' => (new Top())->name(),
                'screen' => (new Mobile())->name(),
                'style' => (new Blue())->name()
            ]
        ];
    }
}