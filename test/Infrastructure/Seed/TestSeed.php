<?php

namespace Popups\Tests\Infrastructure\Seed;

use Popups\Domain\Popup\Position\Bottom;
use Popups\Domain\Popup\Position\Top;
use Popups\Domain\Popup\Screen\Desktop;
use Popups\Domain\Popup\Screen\Mobile;
use Popups\Domain\Popup\Style\Blue;
use Popups\Domain\Popup\Style\Error;
use Popups\Domain\Popup\Style\White;
use Popups\Models\Popup;
use Popups\Models\PopupCondition;
use Popups\Models\PopupContext;
use Popups\Models\PopupQuestion;
use Popups\Models\PopupTag;
use Popups\Models\User;
use Ramsey\Uuid\Uuid;

class TestSeed
{
    public function run()
    {
        foreach ($this->popups() as $modelRow) {
            $model = new Popup($modelRow);
            $model->save();

            $c = new PopupContext();
            $c->popup_id = $model->id;
            $c->context_id = '01000000-a778-268e-3417-589b7a7a49a3';
            $c->save();

            $c = new PopupTag();
            $c->popup_id = $model->id;
            $c->tag_id = '01000000-a778-268e-3417-589b7a7a49aa';
            $c->save();

            $c = new PopupCondition();
            $c->popup_id = $model->id;
            $c->pattern = 'askjdsjajd';
            $c->save();

            $c = new PopupQuestion();
            $c->popup_id = $model->id;
            $c->id = Uuid::uuid4();
            $c->title = 'Вопрос';
            $c->type = 'text';
            $c->save();
        }
        $model = new User($this->user());
        $model->save();
    }

    private function user(): array
    {
        return [
            'unti_id' => '141',
            'email' => 'kuteiko@mail.ru',
            'image' => '',
        ];
    }

    private function popups(): array
    {
        return [
            [
                'id' => '01000000-a778-268e-3417-589b7a7a49d3',
                'title' => 'Окно 1',
                'text' => 'aaa',
                'show_button' => 1,
                'blackout' => 1,
                'is_close' => 1,
                'active' => 1,
                'position' => (new Top())->name(),
                'screen' => (new Mobile())->name(),
                'style' => (new White())->name()
            ],
            [
                'id' => '01000000-a778-268e-3417-589b7a7a49dq',
                'title' => 'Ошибка',
                'text' => 'aaa',
                'show_button' => 1,
                'blackout' => 1,
                'is_close' => 1,
                'active' => 1,
                'position' => (new Bottom())->name(),
                'screen' => (new Desktop())->name(),
                'style' => (new Error())->name()
            ],
            [
                'id' => '01000000-a778-268e-3417-589b7a7a49a3',
                'title' => 'Опрос',
                'text' => 'aaa',
                'show_button' => 1,
                'blackout' => 1,
                'is_close' => 1,
                'active' => 1,
                'position' => (new Top())->name(),
                'screen' => (new Desktop())->name(),
                'style' => (new Blue())->name()
            ]
        ];
    }
}