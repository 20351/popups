<?php

declare(strict_types=1);

namespace Popups\Tests\Infrastructure\Application\Config;

use Kafka\Component\FakeKafka;
use Infrastructure\Application\Config;
use Infrastructure\Application\Database;
use Infrastructure\Application\Language;
use Infrastructure\Environment\Env;
use Popups\Models\Identity\ByAppToken;
use yii\redis\Cache;
use yii\redis\Connection;

class TestConfig implements Config
{
    private Database $database;
    private Language $language;

    public function __construct(Database $database, Language $language)
    {
        $this->database = $database;
        $this->language = $language;
    }

    public function value(): array
    {
        return [
            'id' => 'ru.unti.popups.test',
            'basePath' => dirname(dirname(dirname(dirname(__DIR__)))),
            'language' => $this->language->value(),
            'components' => [
                'db' => $this->database->value(),
                'request' => [
                    'enableCookieValidation' => false,
                ],
                'user' => [
                    'identityClass' => new ByAppToken(),
                    'enableSession' => 'false',
                ],
                'redis' => [
                    'class' => Connection::class, // or may be used \yii\caching\DummyCache
                    'hostname' => (new Env('REDIS_HOST'))->value(),
                    'port' => (new Env('REDIS_PORT'))->value(),
                    'database' => (new Env('REDIS_INDEX'))->value(),
                ],
                'cache' => [
                    'class' => Cache::class,
                    'keyPrefix' => 'POPUPS',
                ],
                'kafka' => [
                    'class' => FakeKafka::class,
                ],
            ],
        ];
    }
}