<?php

declare(strict_types=1);

namespace Popups\Tests\Infrastructure\Application;

use Infrastructure\Application;
use Infrastructure\Application\Database\MariaDb;
use Infrastructure\Application\Language\English;
use Popups\Tests\Infrastructure\Application\Config\TestConfig;
use yii\base\Application as BaseApplication;
use yii\web\Application as WebApplication;

class TestApplication implements Application
{
	public function application(): BaseApplication
	{
        return
            new WebApplication(
              (new TestConfig(
                  new MariaDb(),
                  new English()
              ))
                  ->value()
            );
	}
}