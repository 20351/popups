<?php

declare(strict_types=1);

namespace Popups\Tests\Infrastructure\Http\Transport;

use Generic\Result;
use Generic\Result\Successful;
use Http\Request;
use Http\Transport;
use Popups\Request\User\UserTags;

class FakeUser implements Transport
{
    public function response(Request $request) : Result
    {
        switch (true) {
            case $request instanceof UserTags:
                return new Successful(['body' => $this->data()]);
        }

        return new Successful(['body' => '']);
    }

    private function data() : string
    {
        return <<<response
{
    "unti_id": 141,
    "data": null,
    "current_context": null,
    "level": 2,
    "contexts": [],
    "tags": [
        {
            "uuid": "b5a31d6e-5db4-4174-ac65-26d6206cbbfb"
        }
    ],
    "properties_data": [
        {
            "uuid": "0af00b8e-2429-49e3-8c4e-a1fb67368b87",
            "guid": "luzer2",
            "value": "правильный"
        }
    ]
}
response;
    }


}