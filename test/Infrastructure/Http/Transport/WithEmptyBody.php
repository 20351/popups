<?php

declare(strict_types=1);

namespace Popups\Tests\Infrastructure\Http\Transport;

use Generic\Result;
use Generic\Result\Successful;
use Http\Request;
use Http\Transport;

class WithEmptyBody implements Transport
{
    public function response(Request $request): Result
    {
        return new Successful(['body' => '']);
    }
}