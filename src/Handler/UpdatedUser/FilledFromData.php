<?php

declare(strict_types=1);

namespace Popups\Handler\UpdatedUser;

use Kafka\Format\AsDate;
use Kafka\Models\User;

class FilledFromData
{
    private User $user;
    private array $data;

    public function __construct(User $user, array $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    public function user(): User
    {
        $this->user->name = $this->data['userinfo']['username'] ?? null;
        $this->user->profile = isset($this->data['userinfo']['username']) ? $this->data['userinfo']['username'] . '@uni2035' : null;
        $this->user->image = $this->fromSomeSizes();
        $this->user->email = $this->data['userinfo']['email'] ?? null;
        $this->user->lastname = $this->data['userinfo']['lastname'] ?? null;
        $this->user->middlename = $this->data['userinfo']['secondname'] ?? null;
        $this->user->firstname = $this->data['userinfo']['firstname'] ?? null;
        $this->user->hash = isset($this->data['userinfo']['hashes']) ? json_encode($this->data['userinfo']['hashes']) : null;

        return $this->user;
    }

    private function fromSomeSizes(): ?string
    {
        return
            $this->data['userinfo']['image']['Large']
            ?? $this->data['userinfo']['image']['Medium']
            ?? $this->data['userinfo']['image']['Original']
            ?? $this->data['userinfo']['image']['Small']
            ?? null;
    }

}