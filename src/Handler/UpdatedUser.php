<?php

declare(strict_types=1);

namespace Popups\Handler;

use Generic\Result;
use Generic\Result\Failed;
use Generic\Result\Successful;
use Kafka\Handler;
use Kafka\Handler\UpdatedUser\FilledFromData;
use Popups\Models\User;
use Throwable;

class UpdatedUser extends Handler
{
    public function type(): string
    {
        return 'user';
    }

    public function actions(): array
    {
        return ['create', 'update'];
    }

    protected function result(array $message): Result
    {
        if (!isset($message['id'])) {
            return new Failed(['Cannot save user, ID is not defined']);
        }

        if (!isset($message['id']['user'])) {
            return new Failed(['Cannot save user, User in ID is not defined']);
        }

        if (!isset($message['id']['user']['id'])) {
            return new Failed(['Cannot save user, user id is not defined']);
        }

        try {
            $user =
                (new FilledFromData(
                    $this->existedOrNewUser((int)$message['id']['user']['id']),
                    $message['data'] ?? []
                ))
                    ->user();
        } catch (Throwable $exception) {
            return new Failed(['Failed to create/update user', $exception->getMessage()]);
        }

        return
            $user->save()
                ? new Successful([])
                : new Failed($user->getErrors());
    }

    private function existedOrNewUser(int $untiId): User
    {
        $user = User::find()->where(['unti_id' => $untiId])->one();

        if ($user === null) {
            $user = new User();
            $user->unti_id = $untiId;
        }

        return $user;
    }
}
