<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Identity\Specification;

use Popups\Infrastructure\Identity\Specification;
use Yii;

class UserIsAdmin implements Specification
{
    private ?string $contextId;

    public function __construct(?string $contextId)
    {
        $this->contextId = $contextId;
    }

    public function isSatisfied(): bool
    {
        if (Yii::$app->casbin->enforce((string)Yii::$app->user->id, 'admin', '*')) {
            return true;
        }

        if (!is_null($this->contextId) && Yii::$app->casbin->enforce((string)Yii::$app->user->id, 'admin', $this->contextId)) {
            return true;
        }

        return false;
    }
}