<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Identity\Specification;

use Popups\Infrastructure\Identity\Specification;
use Yii;

class IsGlobalAdmin implements Specification
{
    public function isSatisfied(): bool
    {
        if (Yii::$app->casbin->enforce((string)Yii::$app->user->id, 'admin', '*')) {
            return true;
        }

        return false;
    }
}