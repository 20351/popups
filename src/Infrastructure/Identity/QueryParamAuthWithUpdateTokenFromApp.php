<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Identity;

use GuzzleHttp\Client;
use Http\Transport\DefaultHttpTransport;
use Http\Transport\JsonDecoded;
use Popups\Infrastructure\Identity\Request\AppUserByToken;
use yii\filters\auth\QueryParamAuth;
use yii\web\IdentityInterface;
use yii\web\User;

class QueryParamAuthWithUpdateTokenFromApp extends QueryParamAuth
{
    public function authenticate($user, $request, $response)
    {
        $accessToken = $request->get($this->tokenParam);

        if (is_string($accessToken)) {
            $identity = $this->identityWithUpdateToken($user, $accessToken);
            if ($identity !== null) {
                return $identity;
            }
        }
        if ($accessToken !== null) {
            $this->handleFailure($response);
        }

        return null;
    }

    private function identityWithUpdateToken(User $user, string $accessToken): ?IdentityInterface
    {
        $identity = $user->loginByAccessToken($accessToken, static::class);

        if (is_null($identity)) {
            $this->updateToken($accessToken);
            $identity = $user->loginByAccessToken($accessToken, static::class);
        }

        return $identity;
    }

    private function updateToken(string $accessToken)
    {
        (new SavedUser(
            new AppUserByToken($accessToken),
            new JsonDecoded(
                new DefaultHttpTransport(
                    new Client(['http_errors' => false])
                )
            )
        ))
            ->result();
    }
}