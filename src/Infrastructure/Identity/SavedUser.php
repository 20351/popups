<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Identity;

use Generic\Result;
use Generic\Result\Failed;
use Generic\Result\Successful;
use Http\Request;
use Http\Transport;
use Popups\Infrastructure\Identity\Models\ExistingUserByToken;
use Popups\Infrastructure\Identity\Models\NewUserByToken;
use yii\base\Model;
use yii\db\Query;

class SavedUser
{
    private Request $request;
    private Transport $transport;

    public function __construct(Request $request, Transport $transport)
    {
        $this->request = $request;
        $this->transport = $transport;
    }

    public function result(): Result
    {
        $response = $this->transport->response($this->request);

        if (!$response->isSuccessful()) {
            return $response;
        }

        if (!isset($response->value()['unti_id'])) {
            return new Failed(['Cannot find the user, unti_id is not defined']);
        }

        $user = $this->existingOrNewUser($response->value()['unti_id']);

        $user->setAttributes($response->value());

        if (!$user->validate()) {
            return new Failed($user->errors);
        }

        if (!$user->save()) {
            return new Failed($user->errors);
        }

        return new Successful([]);
    }

    private function existingOrNewUser(int $unti_id): Model
    {
        if ((new Query())
            ->from('user')
            ->andWhere(['unti_id' => $unti_id])
            ->exists()) {
           return new ExistingUserByToken();
        }

        return new NewUserByToken();
    }
}