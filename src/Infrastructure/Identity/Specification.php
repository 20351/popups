<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Identity;

interface Specification
{
    public function isSatisfied(): bool;
}