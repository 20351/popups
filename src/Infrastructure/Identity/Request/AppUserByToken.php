<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Identity\Request;

use Http\Request;
use Http\Request\Method\Get;
use Http\Request\Method;
use Http\Request\Url;
use Http\Request\Url\FromString;
use Infrastructure\Environment\UrlEnv;

class AppUserByToken implements Request
{
    private string $accessToken;

    public function __construct(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function method(): Method
    {
        return new Get();
    }

    public function url(): Url
    {
        return
            new FromString(
                sprintf(
                    '%s/api/user/me?access-token=%s',
                    (new UrlEnv('APP_URL'))->value(),
                    $this->accessToken
                )
            );
    }

    public function headers(): array
    {
        return [
            'content-type' => 'application/json',
        ];
    }

    public function body(): string
    {
        return '';
    }
}