<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Identity;

trait OnlyAuthorizedUsers
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuthWithUpdateTokenFromApp::class,
        ];

        return $behaviors;
    }
}