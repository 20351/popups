<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Identity\Models;

use Popups\Models\User;
use yii\base\Model;

class ExistingUserByToken extends Model
{
    public int $unti_id;
    public string $access_token;

    public function rules()
    {
        return [
            ['unti_id', 'integer'],
            ['unti_id', 'required'],
            ['access_token', 'string', 'length' => [1, 255]],
        ];
    }

    public function save()
    {
        $user = User::findOne($this->unti_id);

        if (is_null($user)) {
            return false;
        }

        $user->access_token = $this->access_token;

        return $user->save();
    }
}