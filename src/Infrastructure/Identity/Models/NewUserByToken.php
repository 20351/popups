<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Identity\Models;

use Popups\Models\User;
use yii\base\Model;

class NewUserByToken extends Model
{
    public $unti_id;
    public $username;
    public $email;
    public $firstname;
    public $lastname;
    public $secondname;
    public $image;
    public $hash;
    public $access_token;

    public function rules()
    {
        return [
            ['unti_id', 'required'],
            [['unti_id'], 'integer'],
            ['unti_id', 'unique', 'targetClass' => User::class],
            [['email'], 'string', 'length' => [1, 128]],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::class],
            [['firstname', 'lastname', 'secondname', 'access_token', 'image'], 'string', 'length' => [1, 255]],
            [['hash'], 'string'],
        ];
    }

    public function save()
    {
        $user = new User([
                             'unti_id' => $this->unti_id,
                             'email' => $this->email,
                             'firstname' => $this->firstname,
                             'lastname' => $this->lastname,
                             'middlename' => $this->secondname,
                             'image' => $this->image,
                             'hash' => $this->hash,
                             'access_token' => $this->access_token,
                         ]);

        return $user->save();
    }
}