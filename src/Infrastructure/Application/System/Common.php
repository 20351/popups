<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Application\System;

use Infrastructure\Application\System;

class Common implements System
{
    public function name(): string
    {
        return 'Common';
    }

    public function controllerNamespace(): string
    {
        return '\Popups\Controller\Common';
    }
}