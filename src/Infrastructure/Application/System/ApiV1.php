<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Application\System;

use Infrastructure\Application\System;

class ApiV1 implements System
{
    public function name(): string
    {
        return 'Api';
    }

    public function controllerNamespace(): string
    {
        return '\Popups\Controller\Api\V1';
    }
}