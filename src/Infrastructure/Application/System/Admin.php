<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Application\System;

use Infrastructure\Application\System;

class Admin implements System
{
    public function name(): string
    {
        return 'Admin';
    }

    public function controllerNamespace(): string
    {
        return '\Popups\Controller\Admin';
    }
}