<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Application\Config;

use Casbin\Component\Casbin;
use Elk\Component\Elk;
use Elk\Target\ElkTarget;
use Infrastructure\Application\Config;
use Kafka\Component\Kafka;
use Sentry\Component\Sentry;
use Sentry\Target\SentryTarget;
use Infrastructure\Application\Database;
use Infrastructure\Application\Language;
use Infrastructure\Application\Routes;
use Infrastructure\Application\System;
use yii\i18n\PhpMessageSource;
use yii\web\HtmlResponseFormatter;
use yii\web\JsonParser;
use yii\web\Response;
use yii\web\User;

class Html implements Config
{
    private System $system;
    private Database $database;
    private Language $language;
    private Routes $routes;

    public function __construct(System $system, Database $database, Language $language, Routes $routes)
    {
        $this->database = $database;
        $this->language = $language;
        $this->system = $system;
        $this->routes = $routes;
    }

    public function value(): array
    {
        return [
            'id' => 'ru.unti.popups',
            'basePath' => dirname(dirname(dirname(dirname(__DIR__)))),
            'language' => $this->language->value(),
            'controllerNamespace' => $this->system->controllerNamespace(),
            'bootstrap' => ['log'],
            'modules' => [],
            'components' => [
                'request' => [
                    'enableCookieValidation' => true,
                    'parsers' => [
                        'application/json' => JsonParser::class,
                    ],
                ],
                'log' => [
                    'traceLevel' => 0,
                    'targets' => [
                        [
                            'class' => SentryTarget::class,
                            'levels' => ['error'],
                            'logVars' => [],
                            'categories' => ['application'],
                        ],
                        [
                            'class' => ElkTarget::class,
                            'levels' => ['info', 'warning', 'error', 'trace'],
                            'logVars' => [],
                            'categories' => ['application'],
                        ]
                    ],
                ],
                'urlManager' => [
                    'enablePrettyUrl' => true,
                    'enableStrictParsing' => true,
                    'showScriptName' => false,
                    'rules' => $this->routes->value(),
                ],
                'response' => [
                    'class' => Response::class,
                    'format' => Response::FORMAT_HTML,
                    'formatters' => [
                        Response::FORMAT_XML => [ // enforce HTML output for browser
                            'class' => HtmlResponseFormatter::class,
                        ],
                    ],
                ],
                'db' => $this->database->value(),
                'i18n' => [
                    'translations' => [
                        '*' => [
                            'class' => PhpMessageSource::class,
                            'sourceLanguage' => 'en',
                            'fileMap' => [
                                'app' => 'app.php',
                            ],
                        ],
                    ],
                ],
                'user' => [
                    'identityClass' => User::class,
                    'enableSession' => 'true',
                ],
                'sentry' => [
                    'class' => Sentry::class,
                ],
                'elk' => [
                    'class' => Elk::class,
                ],
                'kafka' => [
                    'class' => Kafka::class,
                ],

                'casbin' => [
                    'class' => Casbin::class,
                ],

            ],
        ];
    }
}