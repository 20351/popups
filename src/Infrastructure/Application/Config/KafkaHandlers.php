<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Application\Config;

use Infrastructure\Application\Config;
use Kafka\Handler\DeletedCasbinRule;
use Kafka\Handler\UpdatedCasbinRule;
use Kafka\Topic\SSO;
use Popups\Handler\UpdatedUser;

class KafkaHandlers implements Config
{
    public function value(): array
    {
        return [
            new UpdatedUser(new SSO()),
            new UpdatedCasbinRule(new SSO()),
            new DeletedCasbinRule(new SSO()),
        ];
    }
}