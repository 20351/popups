<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Application\Config;

use Casbin\Component\Casbin;
use Infrastructure\Http\Event\BeforeSend;
use Elk\Component\Elk;
use Elk\Target\ElkTarget;
use Infrastructure\Application\Config;
use Kafka\Component\Kafka;
use Sentry\Component\Sentry;
use Sentry\Target\SentryTarget;
use Infrastructure\Application\Database;
use Infrastructure\Application\Language;
use Infrastructure\Application\Routes;
use Infrastructure\Application\System;
use Infrastructure\Environment\Env;
use yii\i18n\PhpMessageSource;
use yii\redis\Cache;
use yii\redis\Connection;
use yii\web\IdentityInterface;
use yii\web\JsonParser;
use yii\web\JsonResponseFormatter;
use yii\web\Response;

class Restful implements Config
{
    private System $system;
    private Database $database;
    private Language $language;
    private Routes $routes;
    private IdentityInterface $identityClass;

    public function __construct(System $system, Database $database, Language $language, Routes $routes, IdentityInterface $identityClass)
    {
        $this->database = $database;
        $this->language = $language;
        $this->system = $system;
        $this->routes = $routes;
        $this->identityClass = $identityClass;
    }

    public function value(): array
    {
        return [
            'id' => 'ru.unti.popups',
            'basePath' => dirname(dirname(dirname(dirname(__DIR__)))),
            'language' => $this->language->value(),
            'controllerNamespace' => $this->system->controllerNamespace(),
            'bootstrap' => ['log'],
            'modules' => [],
            'components' => [
                'request' => [
                    'enableCookieValidation' => false,
                    'parsers' => [
                        'application/json' => JsonParser::class,
                    ],
                ],
                'log' => [
                    'traceLevel' => 0,
                    'targets' => [
                        [
                            'class' => SentryTarget::class,
                            'levels' => ['error'],
                            'logVars' => [],
                            'categories' => ['application'],
                        ],
                        [
                            'class' => ElkTarget::class,
                            'levels' => ['info', 'warning', 'error', 'trace'],
                            'logVars' => [],
                            'categories' => ['application'],
                        ]
                    ],
                ],
                'urlManager' => [
                    'enablePrettyUrl' => true,
                    'enableStrictParsing' => true,
                    'showScriptName' => false,
                    'rules' => $this->routes->value(),
                ],
                'response' => [
                    'class' => Response::class,
                    'on beforeSend' => new BeforeSend(),
                    'format' => Response::FORMAT_JSON,
                    'formatters' => [
                        Response::FORMAT_XML => [ // enforce JSON output for browser
                            'class' => JsonResponseFormatter::class,
                            'prettyPrint' => YII_DEBUG,
                            'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                        ],
                    ],
                ],
                'db' => $this->database->value(),
                'i18n' => [
                    'translations' => [
                        '*' => [
                            'class' => PhpMessageSource::class,
                            'sourceLanguage' => 'en',
                            'fileMap' => [
                                'app' => 'app.php',
                            ],
                        ],
                    ],
                ],
                'user' => [
                    'identityClass' => $this->identityClass,
                    'enableSession' => 'false',
                ],
                'redis' => [
                    'class' => Connection::class,
                    'hostname' => (new Env('REDIS_HOST'))->value(),
                    'port' => (new Env('REDIS_PORT'))->value(),
                    'database' => (new Env('REDIS_INDEX'))->value(),
                ],
                'cache' => [
                    'class' => Cache::class,
                    'keyPrefix' => 'POPUPS',
                ],
                'casbin' => [
                    'class' => Casbin::class,
                ],
                'sentry' => [
                    'class' => Sentry::class,
                ],
                'elk' => [
                    'class' => Elk::class,
                ],
                'kafka' => [
                    'class' => Kafka::class,
                ],
            ],
        ];
    }
}