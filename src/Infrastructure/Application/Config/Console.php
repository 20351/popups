<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Application\Config;

use Casbin\Component\Casbin;
use Infrastructure\Environment\Env;
use Popups\Controller\Console\AppTokenController;
use Popups\Controller\Console\ImportController;
use Popups\Controller\Console\RecreateController;
use Elk\Component\Elk;
use Elk\Target\ElkTarget;
use Infrastructure\Application\Config;
use Infrastructure\Application\Database;
use Kafka\Component\Kafka as KafkaComponent;
use Kafka\Controller\KafkaController;
use Sentry\Component\Sentry;
use Sentry\Target\SentryTarget;
use yii\console\controllers\MigrateController;
use yii\i18n\PhpMessageSource;
use yii\redis\Cache;
use yii\redis\Connection;

class Console implements Config
{
    private Database $database;
    private Config $kafkaConfig;

    public function __construct(Database $database, Config $kafkaConfig)
    {
        $this->database = $database;
        $this->kafkaConfig = $kafkaConfig;
    }

    public function value(): array
    {
        return [
            'id' => 'ru.unti.popups.console',
            'basePath' => dirname(dirname(dirname(dirname(__DIR__)))),
            'language' => 'en',
            'controllerMap' => [
                'migrate' => [
                    'class' => MigrateController::class,
                    'migrationPath' => [
                        __DIR__ . '/../../../../migrations',
                    ],
                    'migrationTable' => '_migration',
                ],
                'kafka' => [
                    'class' => KafkaController::class,
                ],
                'import'  => [
                    'class' => ImportController::class,
                ],
                'recreate'  => [
                    'class' => RecreateController::class,
                ],
                'appToken'  => [
                    'class' => AppTokenController::class,
                ],
            ],
            'components' => [
                'db' => $this->database->value(),
                'log' => [
                    'traceLevel' => YII_DEBUG ? 3 : 0,
                    'targets' => [
                        [
                            'class' => SentryTarget::class,
                            'levels' => ['error'],
                            'logVars' => [],
                            'categories' => ['application'],
                        ],
                        [
                            'class' => ElkTarget::class,
                            'levels' => ['info', 'warning', 'error'],
                            'logVars' => [],
                            'categories' => ['application'],
                        ]
                    ],
                ],
                'i18n' => [
                    'translations' => [
                        '*' => [
                            'class' => PhpMessageSource::class,
                            'sourceLanguage' => 'en',
                            'fileMap' => [
                                'app' => 'app.php',
                            ],
                        ],
                    ],
                ],
                'redis' => [
                    'class' => Connection::class,
                    'hostname' => (new Env('REDIS_HOST'))->value(),
                    'port' => (new Env('REDIS_PORT'))->value(),
                    'database' => (new Env('REDIS_INDEX'))->value(),
                ],
                'cache' => [
                    'class' => Cache::class,
                    'keyPrefix' => 'POPUPS',
                ],
                'sentry' => [
                    'class' => Sentry::class,
                ],
                'elk' => [
                    'class' => Elk::class,
                ],
                'kafka' => [
                    'class' => KafkaComponent::class,
                    'handlers' => $this->kafkaConfig->value(),
                ],
                'casbin' => [
                    'class' => Casbin::class,
                ]

            ],
        ];
    }
}