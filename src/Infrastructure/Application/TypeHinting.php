<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Application;

use Elk\Component\Elk;
use Kafka\Component\Kafka;
use Sentry\Component\Sentry;
use yii\web\Application;

class TypeHinting extends Application
{
    /**
     * @var Sentry $sentry
     */
    public $sentry;

    /**
     * @var Elk $elk
     */
    public $elk;

    /**
     * @var Kafka $kafka
     */
    public $kafka;
}