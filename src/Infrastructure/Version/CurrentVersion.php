<?php

declare(strict_types=1);

namespace Popups\Infrastructure\Version;

class CurrentVersion
{
    public function value(): string
    {
        return '0.1.0';
    }
}