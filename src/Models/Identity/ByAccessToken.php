<?php

declare(strict_types=1);

namespace Popups\Models\Identity;

use Infrastructure\Model\Traits\FindOnlyNotDeleted;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property integer $unti_id
 */
class ByAccessToken extends ActiveRecord implements IdentityInterface
{
    use FindOnlyNotDeleted;

    public static function tableName()
    {
        return 'user';
    }

    public static function findIdentity($id)
    {
        return null;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->unti_id;
    }

    public function getAuthKey()
    {
        return null;
    }

    public function validateAuthKey($authKey)
    {
        return null;
    }
}