<?php

declare(strict_types=1);

namespace Popups\Models\Admin;

use yii\db\ActiveQuery;


class Popup extends \Popups\Models\Popup
{
    public function fields()
    {
        return [
            'id',
            'title',
            'text',
            'url',
            'image',
            'video',
            'show_button',
            'position',
            'screen',
            'style',
            'blackout',
            'is_close',
            'show_one_user',
            'show_guest',
            'show_without_reaction',
            'number_launch',
            'start_dt',
            'end_dt',
            'active',
            'is_deleted',
            'contexts' => function () {
                return $this->contexts;
            },
            'tags' => function () {
                return $this->tags;
            },
            'conditions' => function () {
                return $this->condition;
            },
            'questions' => function () {
                return $this->questions;
            },

        ];
    }

    public function getContexts(): ActiveQuery
    {
        return $this->hasMany(PopupContext::class, ['popup_id' => 'id']);
    }

    public function getTags(): ActiveQuery
    {
        return $this->hasMany(PopupTag::class, ['popup_id' => 'id']);
    }

    public function getCondition(): ActiveQuery
    {
        return $this->hasMany(PopupCondition::class, ['popup_id' => 'id']);
    }

    public function getQuestions(): ActiveQuery
    {
        return $this->hasMany(PopupQuestion::class, ['popup_id' => 'id']);
    }
}