<?php

declare(strict_types=1);

namespace Popups\Models\Admin;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class PopupQuestion extends \Popups\Models\PopupQuestion
{
    public function fields()
    {
        return [
            'id',
            'popup_id',
            'title',
            'type'
        ];
    }
}