<?php

declare(strict_types=1);

namespace Popups\Models\Admin;

class PopupTag extends \Popups\Models\PopupTag
{
    public function fields()
    {
        return [
            'popup_id',
            'tag_id',
            'tag_guid'
        ];
    }
}