<?php

declare(strict_types=1);

namespace Popups\Models\Admin;

class PopupCondition extends \Popups\Models\PopupCondition
{
    public function fields()
    {
        return [
            'popup_id',
            'pattern'
        ];
    }
}