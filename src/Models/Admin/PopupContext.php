<?php

declare(strict_types=1);

namespace Popups\Models\Admin;

class PopupContext extends \Popups\Models\PopupContext
{
    public function fields()
    {
        return [
            'popup_id',
            'context_id',
            'context_guid'
        ];
    }
}