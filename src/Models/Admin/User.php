<?php

declare(strict_types=1);

namespace Popups\Models\Admin;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;


class User extends \Popups\Models\User
{

    public function fields()
    {
        return [
            'unti_id',
            'firstname',
            'lastname',
            'middlename',
            'email',
            'hash',
            'image',
        ];
    }
}
