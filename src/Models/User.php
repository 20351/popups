<?php

declare(strict_types=1);

namespace Popups\Models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * @property integer $unti_id
 * @property string $email
 * @property boolean $is_deleted
 * @property string $lastname
 * @property string $firstname
 * @property string $middlename
 * @property string $gender
 * @property string $created_at
 * @property string $updated_at
 * @property string $access_token
 *
 * @mixin TimestampBehavior
 * @mixin SoftDeleteBehavior
 */

class User extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            [['unti_id'], 'integer'],
            [['email'], 'string', 'length' => [6, 128]],
            [['email'], 'unique'],
        ];
    }

    public static function tableName()
    {
        return 'user';
    }
    public static function primaryKey()
    {
        return ['unti_id'];
    }
}
