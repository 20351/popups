<?php

declare(strict_types=1);

namespace Popups\Models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * @property string $popup_id
 * @property string $context_id
 * @property string $context_guid
 * @property string $created_at
 */
class PopupContext extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
        ];
    }
    public static function primaryKey(): array
    {
        return [
            'popup_id',
            'context_id',
        ];
    }
}