<?php

declare(strict_types=1);

namespace Popups\Models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * @property string $token
 * @property string $application
 * @property string $created_at
 */
class AppToken extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    public function getId()
    {
        return $this->token;
    }

    public function rules()
    {
        return [
            [['token'], 'string', 'length' => [20, 255]],
            [['application'], 'string', 'max' => 255],
        ];
    }

    public static function tableName()
    {
        return 'app_token';
    }
}