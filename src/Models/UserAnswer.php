<?php

declare(strict_types=1);

namespace Popups\Models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * @property string $question_id
 * @property string $unti_id
 * @property string $guest_id
 * @property string $answer
 * @property string $created_at
 */
class UserAnswer extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

}