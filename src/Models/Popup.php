<?php

declare(strict_types=1);

namespace Popups\Models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * @property string $id
 * @property string $title
 * @property string $text
 * @property string $url
 * @property string $image
 * @property string $video
 * @property string $position
 * @property string $style
 * @property string $screen
 * @property string $button
 * @property integer $show_button
 * @property integer $show_guest
 * @property integer $blackout
 * @property integer $is_close
 * @property integer $show_without_reaction
 * @property integer $show_one_user
 * @property integer $number_launch
 * @property string $start_dt
 * @property string $end_dt
 * @property integer $active
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $updated_at
 *
 * @mixin TimestampBehavior
 * @mixin SoftDeleteBehavior
 *
 */
class Popup extends ActiveRecord
{
    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            [['number_launch', 'show_without_reaction', 'show_one_user'], 'integer'],
            [['blackout','is_close','show_guest','show_button'], 'boolean'],
            [['title', 'text', 'video', 'image', 'url'], 'trim'],
            [['title', 'text', 'video', 'image', 'url', 'position', 'style', 'screen', 'button'], 'string'],
            [['start_dt', 'end_dt'], 'safe'],
        ];
    }

    public static function tableName()
    {
        return 'popup';
    }

    public function getContexts(): ActiveQuery
    {
        return $this->hasMany(PopupContext::class, ['popup_id' => 'id']);
    }

    public function getTags(): ActiveQuery
    {
        return $this->hasMany(PopupTag::class, ['popup_id' => 'id']);
    }

    public function getCondition(): ActiveQuery
    {
        return $this->hasMany(PopupCondition::class, ['popup_id' => 'id']);
    }

    public function getUserLogs(): ActiveQuery
    {
        return $this->hasMany(PopupUpdatedLog::class, ['popup_id' => 'id']);
    }

}