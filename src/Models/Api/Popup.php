<?php

declare(strict_types=1);

namespace Popups\Models\Api;

use Popups\Models\Admin\PopupContext;
use Popups\Models\UserTrackerLog;
use yii\db\ActiveQuery;


class Popup extends \Popups\Models\Popup
{
    public $tracker_uuid;
    public function fields()
    {
        return [
            'id',
            'title',
            'text',
            'url',
            'image',
            'video',
            'button',
            'show_button',
            'position',
            'screen',
            'style',
            'blackout',
            'is_close',
            'tracker_uuid',
            'questions' => function () {
                return $this->questions;
            },
        ];
    }

    public function getQuestions(): ActiveQuery
    {
        return $this->hasMany(PopupQuestion::class, ['popup_id' => 'id']);
    }


}