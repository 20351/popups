<?php

declare(strict_types=1);

namespace Popups\Models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * @property string $popup_id
 * @property string $unti_id
 * @property string $created_at
 */
class PopupUpdatedLog extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
        ];
    }
}