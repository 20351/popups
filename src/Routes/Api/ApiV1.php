<?php

declare(strict_types=1);

namespace Popups\Routes\Api;

use Infrastructure\Application\Routes;

class ApiV1 implements Routes
{
    public function value(): array
    {
        return [
            'GET,OPTIONS /v1/version' => 'site/version',

            'GET,OPTIONS /v1/popups' => 'popup/list',
            'POST,OPTIONS /v1/popup/<id>/reaction' => 'popup/create-reaction',
        ];
    }
}