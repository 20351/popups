<?php

declare(strict_types=1);

namespace Popups\Routes;

use Infrastructure\Application\Routes;

class Common implements Routes
{
    public function value(): array
    {
        return [
            'GET /swagger/v1' => 'swagger/v1-export',
            'GET /doc/v1' => 'swagger/v1-doc',
            'GET /swagger/admin' => 'swagger/admin-export',
            'GET /doc/admin' => 'swagger/admin-doc',
        ];
    }
}