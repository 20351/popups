<?php

declare(strict_types=1);

namespace Popups\Routes;

use Infrastructure\Application\Routes;

class Admin implements Routes
{
    public function value(): array
    {
        return [
            'GET /version' => 'site/version',

            // OPTIONS need for CORS requests
            'GET,OPTIONS /me' => 'user/index',

            'GET,OPTIONS /popups' => 'popup/list',
            'GET,OPTIONS /popups/<id>' => 'popup/view',
            'POST,OPTIONS /popups' => 'popup/create',
            'PATCH,OPTIONS /popups' => 'popup/update',
            'DELETE,OPTIONS /popups/<id:[\w-]+>' => 'popup/delete',
        ];
    }
}
