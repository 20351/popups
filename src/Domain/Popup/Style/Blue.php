<?php

namespace Popups\Domain\Popup\Style;

use Popups\Domain\Popup\Style;

class Blue implements Style
{

    public function name(): string
    {
        return 'blue';
    }
}