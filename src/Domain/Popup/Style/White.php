<?php

namespace Popups\Domain\Popup\Style;

use Popups\Domain\Popup\Style;

class White implements Style
{

    public function name() : string
    {
        return 'white';
    }
}