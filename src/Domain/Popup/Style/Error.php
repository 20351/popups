<?php

namespace Popups\Domain\Popup\Style;

use Popups\Domain\Popup\Style;

class Error implements Style
{

    public function name() : string
    {
        return 'error';
    }
}