<?php

namespace Popups\Domain\Popup;

use Generic\Response;

interface Screen
{
    public function name(): string;
}