<?php

namespace Popups\Domain\Popup;

use Generic\Response;

interface Style
{
    public function name(): string;
}