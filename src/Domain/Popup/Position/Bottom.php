<?php

namespace Popups\Domain\Popup\Position;

use Popups\Domain\Popup\Position;

class Bottom implements Position
{

    public function name() : string
    {
        return 'bottom';
    }
}