<?php

namespace Popups\Domain\Popup\Position;

use Popups\Domain\Popup\Position;

class Left implements Position
{

    public function name() : string
    {
        return 'left';
    }
}