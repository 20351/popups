<?php

namespace Popups\Domain\Popup\Position;

use Popups\Domain\Popup\Position;

class Top implements Position
{

    public function name() : string
    {
        return 'top';
    }
}