<?php

namespace Popups\Domain\Popup\Position;

use Popups\Domain\Popup\Position;

class Right implements Position
{

    public function name() : string
    {
        return 'right';
    }
}