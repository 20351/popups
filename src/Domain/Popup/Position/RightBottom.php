<?php

namespace Popups\Domain\Popup\Position;

use Popups\Domain\Popup\Position;

class RightBottom implements Position
{

    public function name() : string
    {
        return 'right_bottom';
    }
}