<?php

namespace Popups\Domain\Popup\Screen;

use Popups\Domain\Popup\Screen;

class Mobile implements Screen
{

    public function name(): string
    {
        return 'mobile';
    }
}