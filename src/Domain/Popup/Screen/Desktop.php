<?php

namespace Popups\Domain\Popup\Screen;

use Popups\Domain\Popup\Screen;

class Desktop implements Screen
{

    public function name(): string
    {
        return 'desktop';
    }
}