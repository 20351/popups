<?php

namespace Popups\Domain\Popup;

use Generic\Response;

interface Position
{
    public function name(): string;
}