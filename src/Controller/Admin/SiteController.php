<?php

declare(strict_types=1);

namespace Popups\Controller\Admin;

use Popups\UserStory\ExternalApplication\ViewVersion\ViewVersion;
use Infrastructure\Identity\OnlyRegisteredApps;
use yii\rest\Controller;

/**
 *  @OA\OpenApi(
 *      @OA\Info(
 *          title="Popups application Admin",
 *          version="1.0"
 *      ),
 *      @OA\Server(
 *          url="https://api.u2035dev.ru/popups/admin",
 *          description="Dev"
 *     ),
 *      @OA\Server(
 *          url="https://api.u2035test.ru/popups/admin",
 *          description="Test"
 *     ),
 *      @OA\Server(
 *          url="https://api.u2035stage.ru/popups/admin",
 *          description="Stage"
 *     ),
 *      @OA\Server(
 *          url="https://api.2035.university/popups/admin",
 *          description="Prod"
 *     ),
 * )
 */
class SiteController extends Controller
{
    /**
     * @OA\Get(
     *     path="/version",
     *     operationId="viewVersion",
     *     summary="View current application version",
     *     @OA\Parameter(
     *         name="app_token",
     *         in="query",
     *         description="Application token",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="OK",
     *        @OA\JsonContent(
     *          example=
     *          {
     *              "result": {
     *                  "successful": true,
     *                  "code": 200
     *              },
     *              "payload": {
     *                 "version": "0.1.0"
     *              }
     *           }
     *        ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized user",
     *     ),
     * )
     */
    public function actionVersion()
    {
        return new ViewVersion();
    }
}
