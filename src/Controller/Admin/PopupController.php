<?php

declare(strict_types=1);

namespace Popups\Controller\Admin;

use Infrastructure\UserStory;
use Infrastructure\UserStory\AccessForbidden;
use Popups\Infrastructure\Identity\OnlyAuthorizedUsers;
use Popups\Infrastructure\Identity\Specification\UserIsAdmin;
use Popups\Models\User;
use Popups\UserStory\Admin\CreatePopup\CreatePopup;
use Popups\UserStory\Admin\CreatePopup\Validation\ValidatedCreatePopupCommand;
use Popups\UserStory\Admin\DeletePopup\DeletePopup;
use Popups\UserStory\Admin\DeletePopup\Validation\ValidatedDeletePopupCommand;
use Popups\UserStory\Admin\UpdatePopup\UpdatePopup;
use Popups\UserStory\Admin\UpdatePopup\Validation\ValidatedUpdatePopupCommand;
use Popups\UserStory\Admin\ViewPopup\Validation\ValidatedViewPopupCommand;
use Popups\UserStory\Admin\ViewPopup\ViewPopup;
use Popups\UserStory\Admin\ViewPopupsList\Validation\ValidatedViewPopupsListCommand;
use Popups\UserStory\Admin\ViewPopupsList\ViewPopupsList;
use Yii;
use yii\rest\Controller;

class PopupController extends Controller
{
    use OnlyAuthorizedUsers;

    /**
     * @OA\Get(
     *     path="/popups",
     *     operationId="viewPopupsList",
     *     summary="List popups",
     *     @OA\Parameter(
     *        name="access-token",
     *         in="query",
     *         description="User token",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="OK",
     *        @OA\JsonContent(
     *          example=
     *          {
     *               "result": {
     *                   "successful": true,
     *                   "code": 200
     *                },
     *                "payload": {
     *                   {
     *                       "id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                       "title": "One",
     *                       "text": "One",
     *                        "url": null,
     *                        "image": null,
     *                       "video": null,
     *                       "show_button": 0,
     *                       "position": "top",
     *                       "screen": "desktop",
     *                     "style": "white",
     *                     "blackout": 0,
     *                           "is_close": 1,
     *                       "show_one_user": 5,
     *                       "show_guest": 0,
     *                       "show_without_reaction": 5,
     *                       "number_launch": 0,
     *                           "start_dt": null,
     *                       "end_dt": null,
     *                       "active": 1,
     *                       "is_deleted": 0,
     *                       "contexts": {
     *                           {
     *                               "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                               "context_id": "316d4659-51bd-11ec-9f1d-0242ac120003",
     *                               "context_guid": null
     *                           }
     *                        },
     *                       "tags": {
     *                           {
     *                               "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                               "tag_id": "316d4659-51bd-11ec-9f1d-0242ac120003",
     *                               "tag_guid": null
     *                               }
     *                          },
     *                       "conditions": {
     *                          {
     *                               "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                               "pattern": "http://localhost/"
     *                           }
     *                       },
     *                      "questions": {
     *                           {
     *                               "id": "d2b5c24b-4d1b-11ec-91c5-0242ac120004",
     *                                "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                                "title": "Вопрос 1",
     *                                "type": "text"
     *                           }
     *                       }
     *                   }
     *               },
     *            "pagination": {
     *                   "total": 1,
     *                  "per_page": 20,
     *                   "page": 1,
     *                   "pages": 1
     *           }
     *
     *       }
     *     )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized user",
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Unauthorized user",
     *     ),
     * )
     */
    public function actionList(): UserStory
    {
        if (!(new UserIsAdmin(null))->isSatisfied()) {
            Yii::warning(
                sprintf(
                    'User with id = `%s` does not have permission',
                    Yii::$app->user->id
                )
            );

            return new AccessForbidden();
        }
        return new ViewPopupsList(
            new ValidatedViewPopupsListCommand(
                Yii::$app->request->get()
            ),
            User::findOne(Yii::$app->user->id)
        );
    }

    /**
     * @OA\Get(
     *     path="/popups/{id}/",
     *     operationId="viewPopup",
     *     summary="View popup",
     *     @OA\Parameter(
     *         name="access-token",
     *         in="query",
     *         description="User token",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Popup uuid",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="OK",
     *        @OA\JsonContent(
     *          example=
     *          {
     *              "result": {
     *                  "successful": true,
     *                  "code": 200
     *              },
     *          *                "payload":
     *                   {
     *                       "id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                       "title": "One",
     *                       "text": "One",
     *                        "url": null,
     *                        "image": null,
     *                       "video": null,
     *                       "show_button": 0,
     *                       "position": "top",
     *                       "screen": "desktop",
     *                     "style": "white",
     *                     "blackout": 0,
     *                           "is_close": 1,
     *                       "show_one_user": 5,
     *                       "show_guest": 0,
     *                       "show_without_reaction": 5,
     *                       "number_launch": 0,
     *                           "start_dt": null,
     *                       "end_dt": null,
     *                       "active": 1,
     *                       "is_deleted": 0,
     *                       "contexts": {
     *                           {
     *                               "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                               "context_id": "316d4659-51bd-11ec-9f1d-0242ac120003",
     *                               "context_guid": null
     *                           }
     *                        },
     *                       "tags": {
     *                           {
     *                               "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                               "tag_id": "316d4659-51bd-11ec-9f1d-0242ac120003",
     *                               "tag_guid": null
     *                               }
     *                          },
     *                       "conditions": {
     *                          {
     *                               "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                               "pattern": "http://localhost/"
     *                           }
     *                       },
     *                      "questions": {
     *                           {
     *                               "id": "d2b5c24b-4d1b-11ec-91c5-0242ac120004",
     *                                "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                                "title": "Вопрос 1",
     *                                "type": "text"
     *                           }
     *                       }
     *
     *               },
     *           }
     *        ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized user",
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Unauthorized user",
     *     ),
     * )
     */
    public function actionView(string $id): UserStory
    {
        if (!(new UserIsAdmin(null))->isSatisfied()) {
            Yii::warning(
                sprintf(
                    'User with id = `%s` does not have permission',
                    Yii::$app->user->id
                )
            );

            return new AccessForbidden();
        }

        return
            new ViewPopup(
                new ValidatedViewPopupCommand(
                    $id
                )
            );
    }

    /**
     * @OA\Patch (
     *     path="/popups/{id}/",
     *     operationId="updatePopup",
     *     summary="Update popup",
     *     @OA\Parameter(
     *         name="access-token",
     *         in="query",
     *         description="User token",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Popup uuid",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="OK",
     *        @OA\JsonContent(
     *          example=
     *          {
     *              "result": {
     *                  "successful": true,
     *                  "code": 200
     *              },
     *          *                "payload": {
     *
     *                       "id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                       "title": "One",
     *                       "text": "One",
     *                        "url": null,
     *                        "image": null,
     *                       "video": null,
     *                       "show_button": 0,
     *                       "position": "top",
     *                       "screen": "desktop",
     *                     "style": "white",
     *                     "blackout": 0,
     *                           "is_close": 1,
     *                       "show_one_user": 5,
     *                       "show_guest": 0,
     *                       "show_without_reaction": 5,
     *                       "number_launch": 0,
     *                           "start_dt": null,
     *                       "end_dt": null,
     *                       "active": 1,
     *                       "is_deleted": 0,
     *                       "contexts": {
     *                           {
     *                               "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                               "context_id": "316d4659-51bd-11ec-9f1d-0242ac120003",
     *                               "context_guid": null
     *                           }
     *                        },
     *                       "tags": {
     *                           {
     *                               "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                               "tag_id": "316d4659-51bd-11ec-9f1d-0242ac120003",
     *                               "tag_guid": null
     *                               }
     *                          },
     *                       "conditions": {
     *                          {
     *                               "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                               "pattern": "http://localhost/"
     *                           }
     *                       },
     *                      "questions": {
     *                           {
     *                               "id": "d2b5c24b-4d1b-11ec-91c5-0242ac120004",
     *                                "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                                "title": "Вопрос 1",
     *                                "type": "text"
     *                           }
     *                       }
     *
     *               },
     *           }
     *        ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized user",
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Unauthorized user",
     *     ),
     * )
     */
    public function actionUpdate(string $id): UserStory
    {
        if (!(new UserIsAdmin(null))->isSatisfied()) {
            Yii::warning(
                sprintf(
                    'User with id = `%s` does not have permission',
                    Yii::$app->user->id
                )
            );

            return new AccessForbidden();
        }

        return
            new UpdatePopup(
                $id,
                new ValidatedUpdatePopupCommand(
                    Yii::$app->request->post()
                ),
                Yii::$app->user->id
            );
    }

    /**
     * @OA\Post(
     *     path="/popups/",
     *     operationId="createPopup",
     *     summary="Create popup",
     *     @OA\Parameter(
     *         name="access-token",
     *         in="query",
     *         description="User token",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Popup uuid",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="OK",
     *        @OA\JsonContent(
     *          example=
     *          {
     *              "result": {
     *                  "successful": true,
     *                  "code": 200
     *              },
     *                          "payload": {
     *
     *                       "id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                       "title": "One",
     *                       "text": "One",
     *                        "url": null,
     *                        "image": null,
     *                       "video": null,
     *                       "show_button": 0,
     *                       "position": "top",
     *                       "screen": "desktop",
     *                     "style": "white",
     *                     "blackout": 0,
     *                           "is_close": 1,
     *                       "show_one_user": 5,
     *                       "show_guest": 0,
     *                       "show_without_reaction": 5,
     *                       "number_launch": 0,
     *                           "start_dt": null,
     *                       "end_dt": null,
     *                       "active": 1,
     *                       "is_deleted": 0,
     *                       "contexts": {
     *                           {
     *                               "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                               "context_id": "316d4659-51bd-11ec-9f1d-0242ac120003",
     *                               "context_guid": null
     *                           }
     *                        },
     *                       "tags": {
     *                           {
     *                               "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                               "tag_id": "316d4659-51bd-11ec-9f1d-0242ac120003",
     *                               "tag_guid": null
     *                               }
     *                          },
     *                       "conditions": {
     *                          {
     *                               "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                               "pattern": "http://localhost/"
     *                           }
     *                       },
     *                      "questions": {
     *                           {
     *                               "id": "d2b5c24b-4d1b-11ec-91c5-0242ac120004",
     *                                "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                                "title": "Вопрос 1",
     *                                "type": "text"
     *                           }
     *                       }
     *
     *               },
     *           }
     *        ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized user",
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Unauthorized user",
     *     ),
     * )
     */
    public function actionCreate(): UserStory
    {
        if (!(new UserIsAdmin(null))->isSatisfied()) {
            Yii::warning(
                sprintf(
                    'User with id = `%s` does not have permission',
                    Yii::$app->user->id
                )
            );

            return new AccessForbidden();
        }

        return
            new CreatePopup(
                new ValidatedCreatePopupCommand(
                    Yii::$app->request->post()
                ),
                Yii::$app->user->id
            );
    }

    /**
     * @OA\Delete(
     *     path="/rules/{id}",
     *     operationId="deleteRule",
     *     summary="Delete rule",
     *     @OA\Parameter(
     *        name="access-token",
     *         in="query",
     *         description="User token",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Popup ID",
     *         required=true,
     *         @OA\Schema(type="uuid")
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="OK",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Validation error",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized user",
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Unauthorized user",
     *     ),
     * )
     */
    public function actionDelete(string $id): UserStory
    {
        if (!(new UserIsAdmin(null))->isSatisfied()) {
            Yii::warning(
                sprintf(
                    'User with id = `%s` does not have permission',
                    Yii::$app->user->id
                )
            );

            return new AccessForbidden();
        }

        return
            new DeletePopup(
                new ValidatedDeletePopupCommand($id),
            );
    }
}