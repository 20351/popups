<?php

declare(strict_types=1);

namespace Popups\Controller\Api\V1;

use GuzzleHttp\Client;
use Http\Transport\DefaultHttpTransport;
use Infrastructure\UserStory;
use Popups\Infrastructure\Identity\OnlyAuthorizedUsers;
use Popups\UserStory\Api\CreateReaction\CreateReaction;
use Popups\UserStory\Api\CreateReaction\Validation\ValidatedCreateReactionCommand;
use Popups\UserStory\Api\ViewPopupsList\Validation\ValidatedViewPopupsListCommand;
use Popups\UserStory\Api\ViewPopupsList\ViewPopupsList;
use Yii;
use yii\rest\Controller;

class PopupController extends Controller
{
    /**
     * @OA\Post(
     *     path="/popups",
     *     operationId="viewPopupsList",
     *     summary="List popups",
     *     @OA\Parameter(
     *         name="unti_id",
     *         in="query",
     *         description="Unti id ",
     *         required=false,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="url",
     *         in="query",
     *         description="Url",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="guest_id",
     *         in="query",
     *         description="ID не авторизованого",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="context",
     *         in="query",
     *         description="Uuid контекста",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="screen",
     *         in="query",
     *         description="mobile or desktop",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="OK",
     *        @OA\JsonContent(
     *          example=
     *          {
     *               "result": {
     *                       "successful": true,
     *                       "code": 200
     *               },
     *               "payload": {
     *                   {
     *                       "id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                       "title": "One",
     *                       "text": "One",
     *                       "url": null,
     *                       "image": null,
     *                       "video": null,
     *                       "button": null,
     *                       "show_button": 0,
     *                       "position": "top",
     *                       "screen": "desktop",
     *                       "style": "white",
     *                       "blackout": 0,
     *                       "is_close": 1,
     *                       "questions": {
     *                       {
     *                           "id": "d2b5c24b-4d1b-11ec-91c5-0242ac120004",
     *                           "popup_id": "01000000-a778-268e-3417-589b7a7a49a3",
     *                           "title": "Вопрос 1",
     *                           "type": "text"
     *                       }
     *                   }
     *                   }
     *               }
     *           }
     *        ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized user",
     *     ),
     * )
     */
    public function actionList(): UserStory
    {
        return new ViewPopupsList(
            new ValidatedViewPopupsListCommand(
                Yii::$app->request->get()
            ),
            new DefaultHttpTransport(new Client(['http_errors' => false]))
        );
    }

    /**
     * @OA\Post(
     *     path="/popup/{id}/reaction",
     *     operationId="createReaction",
     *     summary="Create reaction",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Reaction uuid",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencode",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="close",
     *                     type="number",
     *                     enum={0,1},
     *                 ),
     *                 @OA\Property(
     *                     property="reaction",
     *                     type="number",
     *                     enum={0,1},
     *                 ),
     *                 @OA\Property(
     *                     property="show",
     *                     type="number",
     *                     enum={0,1},
     *                 ),
     *                 @OA\Property(
     *                     property="answers",
     *                     type="array",
     *                   @OA\Items(
     *                      @OA\Property(
     *                              property="answer",
     *                              type="string",
     *                          ),
     *                          @OA\Property(
     *                              property="question_id",
     *                              type="string",
     *                          ))
     *                 ),
     *                 example={"close":1,"show":1,"reaction":0,
     *     "answers":{{"queston_id": "6aefcd77-b18d-45f5-9fae-65602017d694"},{"answer": "инструмент1"}}
     *     }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *        response=201,
     *        description="OK",
     *        @OA\JsonContent(
     *          example=
     *          {
     *              "result": {
     *                  "successful": true,
     *                  "code": 200
     *              },
     *              "payload": {
     *                 {
     *                    "id": "22560fc1-a852-4c16-abbe-94506ec87e39",
     *                    "close": 1,
     *                    "show": 1,
     *                    "reaction": 1,
     *                 }
     *              }
     *           }
     *        ),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     * )
     */
    public function actionCreateReaction(string $id): UserStory
    {
        return
            new CreateReaction(
                new ValidatedCreateReactionCommand(
                    $id,
                    Yii::$app->request->post()
                )
            );
    }
}