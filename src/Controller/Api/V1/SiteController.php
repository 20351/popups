<?php

declare(strict_types=1);

namespace Popups\Controller\Api\V1;

use Popups\UserStory\ExternalApplication\ViewVersion\ViewVersion;
use Infrastructure\Identity\OnlyRegisteredApps;
use yii\rest\Controller;

/**
 *  @OA\OpenApi(
 *      @OA\Info(
 *          title="Popups application API",
 *          version="1.0"
 *      ),
 *      @OA\Server(
 *          url="https://api.u2035dev.ru/popups/api/v1",
 *          description="Dev"
 *     ),
 *      @OA\Server(
 *          url="https://api.u2035test.ru/popups/api/v1",
 *          description="Test"
 *     ),
 *      @OA\Server(
 *          url="https://api.u2035stage.ru/popups/api/v1",
 *          description="Stage"
 *     ),
 *      @OA\Server(
 *          url="https://api.2035.university/popups/api/v1",
 *          description="Prod"
 *     ),
 * )
 */
class SiteController extends Controller
{

}
