<?php

namespace Popups\Controller\Console;

use Dotenv\Dotenv;
use Popups\Infrastructure\Application\Config\Console as ConsoleConfig;
use Popups\Models\AppToken;
use Infrastructure\Application\Console;
use Infrastructure\Application\Database\MariaDb;
use Infrastructure\Recreate\DropTables;
use Yii;
use yii\console\Controller;
use yii\helpers\Console as ConsoleHelper;

class RecreateController extends Controller
{
    public function actionDev()
    {
        $this->checkEnv();

        (new DropTables(Yii::$app->db))->run();

        Yii::$app->runAction('migrate', ['interactive' => false]);

        $appToken = new AppToken();
        $appToken->token = 'zzz';
        $appToken->application = 'test';
        $appToken->save(false);
    }

    public function actionTest()
    {
        $this->checkEnv();

        (Dotenv::createMutable(dirname(dirname(dirname(__DIR__))), '.env.test'))->load();

        // Recreate Yii application with test db settings
        (new Console(
            new ConsoleConfig(
                new MariaDb()
            )
        ))
            ->application();

        (new DropTables(Yii::$app->db))->run();

        Yii::$app->runAction('migrate', ['interactive' => false]);
    }

    private function checkEnv(): void
    {
        if (!file_exists(dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . '.env.test')) {
            ConsoleHelper::output('Recreate works ONLY on testing environment!');
            exit();
        }
    }
}