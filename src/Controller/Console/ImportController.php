<?php

declare(strict_types=1);

namespace Popups\Controller\Console;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Connection;
use yii\helpers\Console;

class ImportController extends Controller
{
    /**
     * Импорт прав Casbin из API SSO. Настройки SSO_URL и SSO_ACCESS_TOKEN прописываются в env
     */
    public function actionCasbin()
    {
        $result = Yii::$app->casbin->import();

        if (!$result->isSuccessful()) {
            Console::error(implode("\n", $result->error()));

            return ExitCode::DATAERR;
        }

        if (array_key_exists('policies', $result->value())) {
            Console::output('Proceeded policies: ' . $result->value()['policies']);
        }

        return ExitCode::OK;
    }
}