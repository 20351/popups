<?php

declare(strict_types=1);

namespace Popups\UserStory\Api\ViewPopupsList;

use Generic\Response;
use Generic\Response\BadRequest;
use Generic\Response\Ok;
use Http\Transport;
use Infrastructure\UserStory;
use Infrastructure\Validation\Validatable;
use Popups\Models\Api\Popup;
use Popups\Models\UserTrackerLog;
use Popups\Request\User\UserTags;
use Ramsey\Uuid\Uuid;
use Yii;

class ViewPopupsList implements UserStory
{
    private Validatable $validatable;
    private Transport $transport;

    public function __construct(Validatable $validatable, Transport $transport)
    {
        $this->validatable = $validatable;
        $this->transport = $transport;
    }

    public function response(): Response
    {
        $validationResult = $this->validatable->result();
        if (!$validationResult->isSuccessful()) {
            Yii::warning($validationResult->error());

            return new BadRequest($validationResult->error());
        }

        $screen = $validationResult->value()['screen'] ?? null;
        $unti_id = $validationResult->value()['unti_id'] ?? null;
        $context = $validationResult->value()['context'] ?? null;
        $guest_id = $validationResult->value()['guest_id'] ?? null;
        $url = $validationResult->value()['url'] ?? null;

        if ($url == null) {
            return new Response\BadRequest();
        }
        $uri = strtok($url, '?');
        $uri2 = trim($uri, '/');

        $query = Popup::find()->where(['active'=>1]);
        $tags = null;
        if (!empty($unti_id)) {
            $result = $this->transport->response(
                new UserTags($unti_id)
            );
            if ($result->isSuccessful()) {
                $tags = !empty($result->value()['body']) ? json_decode($result->value()['body'], true) : [];
            }
        }

        $query->leftJoin('popup_condition', 'popup_condition.popup_id=popup.id')
            ->andWhere(['or', ['popup_condition.pattern' => $uri], ['popup_condition.pattern' => $uri2]]);

        if (!is_null($screen)) {
            $query->andWhere(['popup.screen' => $screen]);
        }
        if (!is_null($guest_id)) {
            $query->andWhere(['popup.show_guest' => 1]);
        }

        if (!is_null($context)) {
            $query->leftJoin('popup_context', 'popup_context.popup_id=popup.id')
                ->andWhere(['popup_context.context_id' => $context]);
        } else {
            $query->leftJoin('popup_context', 'popup_context.popup_id=popup.id')
                ->andWhere(['popup_context.context_id' => null]);
        }
        if (!empty($tags['tags'])) {
            $uuids = array_column($tags['tags'], 'uuid');

            $query->leftJoin('popup_tag', 'popup_tag.popup_id=popup.id')
                ->andWhere(['or',['popup_tag.tag_id' => $uuids],['popup_tag.tag_id' => null]]);
        } else {
            $query->leftJoin('popup_tag', 'popup_tag.popup_id=popup.id')
                ->andWhere(['popup_tag.tag_id' => null]);
        }
        $query->andWhere(['or',['>','start_dt','NOW()'],['start_dt'=>null]]);
        $query->andWhere(['or',['<','end_dt','NOW()'],['end_dt'=>null]]);
        $popups = $query->all();


        foreach ($popups as $k => $popup) {
            $tracker_uuid= (Uuid::uuid4())->toString();
            $data = [];
            $data[] = [$tracker_uuid, $popup->id, $unti_id, $guest_id, $url, $context];

            if (!empty($data)) {
                Yii::$app->db->createCommand()->batchInsert(
                    UserTrackerLog::tableName(),
                    ['id', 'popup_id', 'unti_id', 'guest_id', 'url', 'context_id'],
                    $data
                )->execute();
                if (!empty($unti_id)) {
                    $count = UserTrackerLog::find()->where(
                        ['popup_id' => $popup->id, 'unti_id' => $unti_id, 'show' => 0]
                    )->count();
                    $countS = UserTrackerLog::find()->where(
                        ['popup_id' => $popup->id, 'unti_id' => $unti_id, 'show' => 1, 'reaction' => 0]
                    )->count();
                    $countR = UserTrackerLog::find()->where(
                        ['popup_id' => $popup->id, 'unti_id' => $unti_id, 'show' => 1, 'reaction' => 1]
                    )->count();
                } else {
                    $count = UserTrackerLog::find()->where(
                        ['popup_id' => $popup->id, 'guest_id' => $guest_id, 'show' => 0]
                    )->count();
                    $countS = UserTrackerLog::find()->where(
                        ['popup_id' => $popup->id, 'guest_id' => $guest_id, 'show' => 1, 'reaction' => 0]
                    )->count();
                    $countR = UserTrackerLog::find()->where(
                        ['popup_id' => $popup->id, 'guest_id' => $guest_id, 'show' => 1, 'reaction' => 1]
                    )->count();
                }

                if ($countR > 0) {
                    unset($popups[$k]);
                }
                if ($popup->number_launch > $count) {
                    unset($popups[$k]);
                }
                if ($popup->show_without_reaction < $countS) {
                    unset($popups[$k]);
                }

                if ($popup->show_one_user < $countS) {
                    unset($popups[$k]);
                }
                if(!empty($popups[$k]))
                {
                    $popups[$k]['tracker_uuid'] = $tracker_uuid;
                }
            }
        }
        return
            new Ok($popups);
    }

}