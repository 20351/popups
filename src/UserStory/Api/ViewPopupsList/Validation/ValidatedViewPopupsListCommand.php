<?php

declare(strict_types=1);

namespace Popups\UserStory\Api\ViewPopupsList\Validation;

use Generic\Result;
use Generic\Result\Failed;
use Generic\Result\Successful;
use Infrastructure\Validation\Validatable;
use Infrastructure\Validation\Verifica\Verifica;

class ValidatedViewPopupsListCommand implements Validatable
{
    private array $values;

    public function __construct(?array $values)
    {
        $this->values = [
            'unti_id' => !empty($values['unti_id']) ? (int) $values['unti_id'] : null,
            'url' => $values['url'] ?? null,
            'context' => $values['context'] ?? null,
            'screen' => $values['screen'] ?? null,
            'guest_id' => $values['guest_id'] ?? null,
        ];
    }

    public function result(): Result
    {
        $validator = new Verifica($this->values);

        $validator->rule('required', ['url']);
        $validator->rule('integer', ['unti_id']);
        $validator->rule('uuid', ['context']);
        $validator->rule('string', ['url', 'screen', 'guest_id']);

        $validator->stopOnFirstFail();

        return $validator->result();
    }
}