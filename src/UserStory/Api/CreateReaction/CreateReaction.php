<?php

declare(strict_types=1);

namespace Popups\UserStory\Api\CreateReaction;

use Generic\Response;
use Generic\Response\BadRequest;
use Generic\Response\Created;
use Generic\Response\ServerError;
use Infrastructure\Response\WithWarning;
use Infrastructure\UserStory;
use Infrastructure\Validation\Validatable;
use Popups\Models\UserAnswer;
use Popups\Models\UserTrackerLog;
use Popups\UserStory\Admin\CreatePopup\Popup\Persistent;
use Popups\UserStory\Admin\CreatePopup\Popup\WithCondition;
use Popups\UserStory\Admin\CreatePopup\Popup\WithContexts;
use Popups\UserStory\Admin\CreatePopup\Popup\WithQuestion;
use Popups\UserStory\Admin\CreatePopup\Popup\WithTags;
use Popups\UserStory\Admin\CreatePopup\Popup\Log;
use Ramsey\Uuid\Uuid;
use Yii;

class CreateReaction implements UserStory
{
    private Validatable $validatable;

    public function __construct(Validatable $validatable)
    {
        $this->validatable = $validatable;
    }

    public function response(): Response
    {
        $validationResult = $this->validatable->result();

        if (!$validationResult->isSuccessful()) {
            return new WithWarning(new BadRequest($validationResult->error()));
        }

        $id = $validationResult->value()['id'];
        $show = $validationResult->value()['show'];
        $close = $validationResult->value()['close'];
        $reaction = $validationResult->value()['reaction'];
        $answers = $validationResult->value()['answers'];


        $result = UserTrackerLog::findOne($id);

        if (!$result) {
            return new Response\NotFound();
        }

        if (!empty($answers)) {
            foreach ($answers as $item) {
                $answer = new UserAnswer();
                $answer->id = Uuid::uuid4();
                $answer->unti_id = $result->unti_id;
                $answer->guest_id = $result->guest_id;
                $answer->question_id = $item['question_id'];
                $answer->answer = $item['answer'];
                $answer->save();
            }
        }

        $result->show = $show;
        $result->skip = $close;
        $result->reaction = $reaction;
        if (!$result->save()) {
            Yii::error($result->error());

            return new ServerError();
        }

        return
            new Created(
                $result->attributes
            );
    }
}