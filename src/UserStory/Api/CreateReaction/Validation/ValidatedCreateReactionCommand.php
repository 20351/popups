<?php

declare(strict_types=1);

namespace Popups\UserStory\Api\CreateReaction\Validation;

use Generic\Result;
use Generic\Result\Failed;
use Generic\Result\Successful;
use Infrastructure\Validation\Validatable;
use Infrastructure\Validation\Verifica\Verifica;

class ValidatedCreateReactionCommand implements Validatable
{
    private array $values;

    public function __construct(string $id, ?array $values)
    {
        $this->values = [
            'close' => $values['close'] ?? 0,
            'reaction' => $values['reaction'] ?? 0,
            'show' => $values['show'] ?? 0,
            'answers' => $values['answers'] ?? [],
            'id' => $id
        ];
    }

    public function result(): Result
    {
        $validator = new Verifica($this->values);

        $validator->rule('required', ['id']);
        $validator->rule('integer', ['show', 'reaction', 'close']);
        $validator->rule('array', ['answers']);

        $validator->stopOnFirstFail();

        return $validator->result();
    }
}