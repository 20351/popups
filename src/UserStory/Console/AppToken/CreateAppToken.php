<?php

declare(strict_types=1);

namespace Popups\UserStory\Console\AppToken;

use Generic\Result;
use Generic\Result\Failed;
use Generic\Result\Successful;
use Infrastructure\Console\Command;
use Popups\Models\AppToken;

class CreateAppToken implements Command
{
    private string $application;
    private string $token;

    public function __construct(string $application, string $token)
    {
        $this->application = $application;
        $this->token = $token;
    }

    public function run(): Result
    {
        $appToken = new AppToken();
        $appToken->token = $this->token;
        $appToken->application = $this->application;

        $result = $appToken->save();

        if (!$result) {
            return new Failed($appToken->getErrorSummary(true));
        }

        return new Successful([sprintf('Token for `%s` created.', $this->application)]);
    }

}