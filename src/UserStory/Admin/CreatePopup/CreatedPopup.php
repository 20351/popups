<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\CreatePopup;

use Generic\Result;

interface CreatedPopup
{
    public function result(): Result;
}