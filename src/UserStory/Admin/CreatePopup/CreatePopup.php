<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\CreatePopup;

use Generic\Response;
use Generic\Response\BadRequest;
use Generic\Response\Created;
use Generic\Response\ServerError;
use Infrastructure\Response\WithWarning;
use Infrastructure\UserStory;
use Infrastructure\Validation\Validatable;
use Popups\UserStory\Admin\CreatePopup\Popup\Persistent;
use Popups\UserStory\Admin\CreatePopup\Popup\WithCondition;
use Popups\UserStory\Admin\CreatePopup\Popup\WithContexts;
use Popups\UserStory\Admin\CreatePopup\Popup\WithQuestion;
use Popups\UserStory\Admin\CreatePopup\Popup\WithTags;
use Popups\UserStory\Admin\CreatePopup\Popup\Log;
use Ramsey\Uuid\Uuid;
use Yii;

class CreatePopup implements UserStory
{
    private Validatable $validatable;
    private int $unti_id;

    public function __construct(Validatable $validatable, int $userId)
    {
        $this->validatable = $validatable;
        $this->unti_id = $userId;
    }

    public function response(): Response
    {
        $validationResult = $this->validatable->result();

        if (!$validationResult->isSuccessful()) {
            return new WithWarning(new BadRequest($validationResult->error()));
        }

        $validation = $validationResult->value();
        $result =
            (new Log(
                new WithQuestion(
                    new WithCondition(
                        new WithTags(
                            new WithContexts(
                                new Persistent(
                                    $validation['id'] ?? Uuid::uuid4(),
                                    $validation
                                ),
                                $validation['contexts'],
                            ),
                            $validation['tags'],
                        ),
                        $validation['conditions'] ?? [],
                    ),
                    $validation['questions'] ?? [],
                ),
                $this->unti_id
            ))
                ->result();

        if (!$result->isSuccessful()) {
            Yii::error($result->error());

            return new ServerError();
        }

        return
            new Created(
                $result->value()[0]->toArray()
            );
    }
}