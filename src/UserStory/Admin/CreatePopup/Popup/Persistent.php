<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\CreatePopup\Popup;

use Generic\Result;
use Generic\Result\Failed;
use Generic\Result\Successful;
use Popups\Models\Admin\Popup;
use Popups\UserStory\Admin\CreatePopup\CreatedPopup;

class Persistent implements CreatedPopup
{
    private string $id;
    private array $values;

    public function __construct(string $id, array $values)
    {
        $this->id = $id;
        $this->values = $values;
    }

    public function result(): Result
    {
        $popup = new Popup();
        $popup->id = $this->id;
        $popup->setAttributes($this->values);
        $popup->save();

        if ($popup->hasErrors()) {
            return new Failed($popup->getErrorSummary(true));
        }

        return new Successful([$popup]);
    }
}