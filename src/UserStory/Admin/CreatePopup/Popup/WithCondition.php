<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\CreatePopup\Popup;

use Generic\Result;
use Generic\Result\Successful;
use Popups\Models\Admin\Popup;
use Popups\Models\PopupCondition;
use Popups\Models\PopupTag;
use Popups\UserStory\Admin\CreatePopup\CreatedPopup;
use yii\helpers\Json;

class WithCondition implements CreatedPopup
{
    private CreatedPopup $createdPopup;
    private array $condition;

    public function __construct(CreatedPopup $createdPopup, array $condition)
    {
        $this->createdPopup = $createdPopup;
        $this->condition = $condition;
    }

    public function result(): Result
    {
        $originResult = $this->createdPopup->result();

        if (!$originResult->isSuccessful()) {
            return $originResult;
        }

        /** @var Popup $popup */
        $popup = $originResult->value()[0];


        foreach ($this->condition as $condition) {
            $actionObjectModel = new PopupCondition();
            $actionObjectModel->popup_id = $popup->id;
            $actionObjectModel->pattern = $condition;
            $actionObjectModel->save();
        }

        $popup->refresh();

        return new Successful([$popup]);
    }
}