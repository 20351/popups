<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\CreatePopup\Popup;

use Generic\Result;
use Generic\Result\Successful;
use Popups\Models\Admin\Popup;
use Popups\Models\PopupQuestion;
use Popups\UserStory\Admin\CreatePopup\CreatedPopup;
use Ramsey\Uuid\Uuid;
use yii\helpers\Json;

class WithQuestion implements CreatedPopup
{
    private CreatedPopup $createdPopup;
    private array $questions;

    public function __construct(CreatedPopup $createdPopup, array $questions)
    {
        $this->createdPopup = $createdPopup;
        $this->questions = $questions;
    }

    public function result(): Result
    {
        $originResult = $this->createdPopup->result();

        if (!$originResult->isSuccessful()) {
            return $originResult;
        }

        /** @var Popup $popup */
        $popup = $originResult->value()[0];

        foreach ($this->questions as $question) {
            $actionObjectModel = new PopupQuestion();
            $actionObjectModel->id = Uuid::uuid4();
            $actionObjectModel->popup_id = $popup->id;
            $actionObjectModel->title = $question['title'];
            $actionObjectModel->type = $question['type'];
            $actionObjectModel->save();
        }

        $popup->refresh();

        return new Successful([$popup]);
    }
}