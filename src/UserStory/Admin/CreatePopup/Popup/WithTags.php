<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\CreatePopup\Popup;

use Generic\Result;
use Generic\Result\Successful;
use Popups\Models\Admin\Popup;
use Popups\Models\PopupTag;
use Popups\UserStory\Admin\CreatePopup\CreatedPopup;
use yii\helpers\Json;

class WithTags implements CreatedPopup
{
    private CreatedPopup $createdPopup;
    private array $tags;

    public function __construct(CreatedPopup $createdPopup, array $tags)
    {
        $this->createdPopup = $createdPopup;
        $this->tags = $tags;
    }

    public function result(): Result
    {
        $originResult = $this->createdPopup->result();

        if (!$originResult->isSuccessful()) {
            return $originResult;
        }

        /** @var Popup $popup */
        $popup = $originResult->value()[0];


        foreach ($this->tags as $tag) {
            $actionObjectModel = new PopupTag();
            $actionObjectModel->popup_id = $popup->id;
            $actionObjectModel->tag_id = $tag;
            $actionObjectModel->save();
        }

        $popup->refresh();

        return new Successful([$popup]);
    }
}