<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\CreatePopup\Popup;

use Generic\Result;
use Generic\Result\Successful;
use Popups\Models\Admin\Popup;
use Popups\Models\PopupUpdatedLog;
use Popups\UserStory\Admin\CreatePopup\CreatedPopup;
use yii\helpers\Json;

class Log implements CreatedPopup
{
    private CreatedPopup $updatedPopup;
    private int $unti_id;

    public function __construct(CreatedPopup $updatedPopup, int $unti_id)
    {
        $this->updatedPopup = $updatedPopup;
        $this->unti_id = $unti_id;
    }

    public function result(): Result
    {
        $originResult = $this->updatedPopup->result();

        if (!$originResult->isSuccessful()) {
            return $originResult;
        }

        /** @var Popup $popup */
        $popup = $originResult->value()[0];

        $triggerObjectModel = new PopupUpdatedLog();
        $triggerObjectModel->popup_id = $popup->id;
        $triggerObjectModel->unti_id = $this->unti_id;
        $triggerObjectModel->save();

        $popup->refresh();

        return new Successful([$popup]);
    }
}