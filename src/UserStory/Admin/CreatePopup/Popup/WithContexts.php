<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\CreatePopup\Popup;

use Generic\Result;
use Generic\Result\Successful;
use Popups\Models\Admin\Popup;
use Popups\Models\PopupContext;
use Popups\UserStory\Admin\CreatePopup\CreatedPopup;

class WithContexts implements CreatedPopup
{
    private CreatedPopup $createdPopup;
    private array $contexts;

    public function __construct(CreatedPopup $createdPopup, array $contexts)
    {
        $this->createdPopup = $createdPopup;
        $this->contexts = $contexts;
    }

    public function result(): Result
    {
        $originResult = $this->createdPopup->result();

        if (!$originResult->isSuccessful()) {
            return $originResult;
        }

        /** @var Popup $popup */
        $popup = $originResult->value()[0];

        foreach ($this->contexts as $context) {
            $triggerObjectModel = new PopupContext();
            $triggerObjectModel->popup_id = $popup->id;
            $triggerObjectModel->context_id = $context;
            $triggerObjectModel->save();
        }

        $popup->refresh();

        return new Successful([$popup]);
    }
}