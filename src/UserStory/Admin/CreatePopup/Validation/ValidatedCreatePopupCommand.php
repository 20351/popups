<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\CreatePopup\Validation;

use Generic\Result;
use Infrastructure\Validation\Validatable;
use Infrastructure\Validation\Verifica\Verifica;
use yii\helpers\Json;

class ValidatedCreatePopupCommand implements Validatable
{
    private ?array $array;

    public function __construct(?array $array)
    {
        $this->array = $array;
    }

    public function result(): Result
    {

        $validator = new Verifica($this->array);

        $validator->rule('required', ['title', 'position', 'style']);
        $validator->rule('string', ['id','title', 'text', 'url', 'image', 'video', 'position', 'screen', 'style'])->message(
            '{field} is not string'
        );
        $validator->rule('integer', ['show_one_user', 'show_without_reaction', 'number_launch'])->message(
            '{field} is not integer'
        );
        $validator->rule('boolean', ['show_button', 'blackout', 'is_close', 'show_guest', 'active'])->message(
            '{field} is not boolean'
        );
        $validator->rule('array', 'contexts')->message('{field} is not array');
        $validator->rule('array', 'tags')->message('{field} is not array');
        $validator->rule('array', 'conditions')->message('{field} is not array');
        $validator->rule('array', 'questions')->message('{field} is not array');
        $validator->stopOnFirstFail();

        return
            $validator->result();
    }

}