<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\DeletePopup;

use Generic\Response;
use Generic\Response\BadRequest;
use Generic\Response\NotFound;
use Generic\Response\Ok;
use Generic\Response\ServerError;
use Generic\Result;
use Generic\Result\Failed;
use Generic\Result\Successful;
use Infrastructure\Response\WithWarning;
use Infrastructure\UserStory;
use Infrastructure\Validation\Validatable;
use Popups\Models\Admin\Popup;
use Throwable;
use Yii;

class DeletePopup implements UserStory
{
    private Validatable $validatable;

    public function __construct(Validatable $validatable)
    {
        $this->validatable = $validatable;
    }

    public function response(): Response
    {
        $validationResult = $this->validatable->result();

        if (!$validationResult->isSuccessful()) {
            return new WithWarning(new BadRequest($validationResult->error()));
        }

        $validation = $validationResult->value();

        try {
            $popup = Popup::findOne([$validation['id']]);
            if (is_null($popup)) {
                return new NotFound();
            }
            $popup->softDelete();
        } catch (Throwable $exception) {
            Yii::error($exception->getMessage());
            return new ServerError();
        }

        return new Ok([]);
    }
}