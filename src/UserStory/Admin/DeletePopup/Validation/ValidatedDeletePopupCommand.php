<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\DeletePopup\Validation;

use Generic\Result;
use Infrastructure\Validation\Validatable;
use Infrastructure\Validation\Verifica\ExistsValidator;
use Infrastructure\Validation\Verifica\Verifica;
use Popups\Models\Admin\Popup;

class ValidatedDeletePopupCommand implements Validatable
{
    private array $values;

    public function __construct(?string $id)
    {
        $this->values = [
            'id' => $id,
        ];
    }

    public function result(): Result
    {
        $validator = new Verifica($this->values);

        $validator->rule('required', ['id']);
        $validator->rule(new ExistsValidator(Popup::class), 'id')->message('Popup with id = `{values}` not exist');

        $validator->stopOnFirstFail();

        return $validator->result();
    }
}