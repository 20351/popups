<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\ViewPopupsList\Validation;

use Generic\Result;
use Generic\Result\Failed;
use Generic\Result\Successful;
use Infrastructure\Validation\Validatable;
use Infrastructure\Validation\Verifica\Verifica;
use yii\base\DynamicModel;

class ValidatedViewPopupsListCommand implements Validatable
{
    private array $values;

    public function __construct(?array $values)
    {
        if (!empty($values['context']) && is_string($values['context'])) {
            $context = explode(',', $values['context']);
        }
        if (!empty($values['tag']) && is_string($values['tag'])) {
            $tag = explode(',', $values['tag']);
        }

        $this->values = [
            'search' => $values['search'] ?? null,
            'position' => $values['position'] ?? null,
            'style' => $values['style'] ?? null,
            'active' => $values['active'] ?? null,
            'screen' => $values['screen'] ?? null,
            'context' => $context ?? [],
            'tag' => $tag ?? [],
        ];
    }

    public function result(): Result
    {
        $validator = new Verifica($this->values);

        $validator->rule('string', ['search', 'position']);
        $validator->rule('string', ['search', 'position']);

        $validator->stopOnFirstFail();

        return $validator->result();
    }
}