<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\ViewPopupsList;

use Generic\Response;
use Generic\Response\BadRequest;
use Generic\Response\Ok;
use Infrastructure\Response\Pagination\FromDataProvider;
use Infrastructure\UserStory;
use Infrastructure\Validation\Validatable;
use Popups\Models\Admin\Popup;
use Popups\Models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

class ViewPopupsList implements UserStory
{
    private Validatable $validatable;
    private User $user;

    public function __construct(Validatable $validatable, User $user)
    {
        $this->validatable = $validatable;
        $this->user = $user;
    }

    public function response(): Response
    {
        $validationResult = $this->validatable->result();
        if (!$validationResult->isSuccessful()) {
            Yii::warning($validationResult->error());

            return new BadRequest($validationResult->error());
        }

        $commaSeparatedValues =
            !empty($validationResult->value()['search'])
                ? explode(',', $validationResult->value()['search'])
                : [];
        $position = $validationResult->value()['position'] ?? null;
        $active = $validationResult->value()['active'] ?? null;
        $style = $validationResult->value()['style'] ?? null;
        $screen = $validationResult->value()['screen'] ?? null;
        $context = $validationResult->value()['context'] ?? [];
        $tag = $validationResult->value()['tag'] ?? [];


        $query = Popup::find();

        $filter = [];
        if (!empty($commaSeparatedValues)) {
            foreach ($commaSeparatedValues as $item) {
                $filter[] = ['like', 'popup.title', trim($item)];
            }
        }

        $query->with('contexts', 'tags');
        if (!empty($filter)) {
            $query->andFilterWhere(array_merge(['or'], $filter));
        }

        if (!empty($context)) {
            $query->leftJoin('popup_context', 'popup_context.popup_id=popup.id')
                ->andFilterWhere(['popup_context.context_id' => $context]);
        }

        if (!empty($tag)) {
            $query->leftJoin('popup_tag', 'tag.popup_id=popup.id')
                ->andFilterWhere(['tag.tag_id' => $tag]);
        }


        if (!is_null($position)) {
            $query->andWhere(['popup.position' => $position]);
        }
        if (!is_null($screen)) {
            $query->andWhere(['popup.screen' => $screen]);
        }
        if (!is_null($style)) {
            $query->andWhere(['popup.style' => $style]);
        }
        if (!is_null($active)) {
            $query->andWhere(['popup.active' => $active]);
        }

        $provider = $this->provider($query);

        return
            new Ok($provider->models, new FromDataProvider($provider));
    }

    private function provider($query): ActiveDataProvider
    {
        return
            new ActiveDataProvider(
                [
                    'query' => $query,
                    'pagination' => new Pagination(
                        [
                            'pageSizeParam' => 'per-page',
                            'pageSizeLimit' => [1, 100],
                        ]
                    ),
                    'sort' => [
                        'defaultOrder' => [
                            'created_at' => SORT_ASC,
                        ],
                        'attributes' => [
                            'id',
                            'title',
                            'position',
                            'created_at',
                        ],
                    ]
                ]
            );
    }
}