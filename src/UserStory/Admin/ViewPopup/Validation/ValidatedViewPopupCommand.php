<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\ViewPopup\Validation;

use Generic\Result;
use Infrastructure\Validation\Validatable;
use Infrastructure\Validation\Verifica\Verifica;

class ValidatedViewPopupCommand implements Validatable
{
    private array $values;

    public function __construct(string $id)
    {
        $this->values = ['id' => $id];
    }

    public function result(): Result
    {
        $validator = new Verifica($this->values);

        $validator->rule('required', ['id']);
        $validator->rule('uuid', ['id']);

        $validator->stopOnFirstFail();

        return $validator->result();
    }
}