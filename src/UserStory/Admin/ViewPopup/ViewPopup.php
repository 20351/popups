<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\ViewPopup;

use Generic\Response;
use Generic\Response\BadRequest;
use Generic\Response\NotFound;
use Generic\Response\Ok;
use Infrastructure\Response\WithWarning;
use Infrastructure\UserStory;
use Infrastructure\Validation\Validatable;
use Popups\Models\Admin\Popup;
use Yii;

class ViewPopup implements UserStory
{
    private Validatable $validatable;

    public function __construct(Validatable $validatable)
    {
        $this->validatable = $validatable;
    }

    public function response(): Response
    {
        $validationResult = $this->validatable->result();

        if (!$validationResult->isSuccessful()) {
            return new WithWarning(new BadRequest($validationResult->error()));
        }

        $validation = $validationResult->value();

        $popup = Popup::findOne(['id' => $validation['id']]);

        if (is_null($popup)) {
            return new NotFound();
        }

        return
            new Ok(
                $popup->toArray()
            );
    }
}