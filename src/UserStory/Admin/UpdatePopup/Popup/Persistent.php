<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\UpdatePopup\Popup;

use Generic\Result;
use Generic\Result\Failed;
use Generic\Result\Successful;
use Popups\Models\Admin\Popup;
use Popups\UserStory\Admin\UpdatePopup\UpdatedPopup;

class Persistent implements UpdatedPopup
{
    private string $id;
    private array $values;

    public function __construct(string $id, array $values)
    {
        $this->id = $id;
        $this->values = $values;
    }

    public function result(): Result
    {
        $popup = Popup::findOne($this->id);
        $popup->title = $this->values['title'] ?? $popup->title;
        $popup->text = $this->values['text'] ?? $popup->text;
        $popup->url = $this->values['url'] ?? $popup->url;
        $popup->image = $this->values['image'] ?? $popup->image;
        $popup->video = $this->values['video'] ?? $popup->video;
        $popup->button = $this->values['video'] ?? $popup->button;
        $popup->show_button = $this->values['show_button'] ?? 0;
        $popup->position = $this->values['position'] ?? $popup->position;
        $popup->screen = $this->values['screen'] ?? $popup->screen;
        $popup->style = $this->values['style'] ?? $popup->style;
        $popup->blackout = $this->values['blackout'] ?? 0;
        $popup->is_close = $this->values['is_close'] ?? 0;
        $popup->show_one_user = $this->values['show_one_user'] ?? $popup->show_one_user;
        $popup->show_guest = $this->values['show_guest'] ?? $popup->show_guest;
        $popup->show_without_reaction = $this->values['show_without_reaction'] ?? $popup->show_without_reaction;
        $popup->number_launch = $this->values['number_launch'] ?? $popup->number_launch;
        $popup->start_dt = $this->values['start_dt'] ?? $popup->start_dt;
        $popup->end_dt = $this->values['end_dt'] ?? $popup->end_dt;
        $popup->active = $this->values['active'] ?? $popup->active;
        $popup->save();

        if ($popup->hasErrors()) {
            return new Failed($popup->getErrorSummary(true));
        }

        return new Successful([$popup]);
    }
}