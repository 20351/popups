<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\UpdatePopup\Popup;

use Generic\Result;
use Generic\Result\Successful;
use Popups\Models\Admin\Popup;
use Popups\Models\PopupCondition;
use Popups\Models\PopupTag;
use Popups\UserStory\Admin\CreatePopup\CreatedPopup;
use Popups\UserStory\Admin\UpdatePopup\UpdatedPopup;
use yii\helpers\Json;

class WithCondition implements UpdatedPopup
{
    private UpdatedPopup $updatedPopup;
    private array $condition;

    public function __construct(UpdatedPopup $updatedPopup, array $condition)
    {
        $this->updatedPopup = $updatedPopup;
        $this->condition = $condition;
    }

    public function result(): Result
    {
        $originResult = $this->updatedPopup->result();

        if (!$originResult->isSuccessful()) {
            return $originResult;
        }

        /** @var Popup $popup */
        $popup = $originResult->value()[0];

        $ids = $this->condition;

        PopupCondition::deleteAll(['and', ['popup_id' => $popup->id], ['not in', 'pattern', $ids]]);
        foreach ($this->condition as $condition) {
            $tool = PopupCondition::find()->where(
                [
                    'popup_id' => $popup->id,
                    'pattern' => $condition
                ]
            )->exists();
            if (!$tool) {
                $actionObjectModel = new PopupCondition();
                $actionObjectModel->popup_id = $popup->id;
                $actionObjectModel->pattern = $condition;
                $actionObjectModel->save();
            }
        }

        $popup->refresh();

        return new Successful([$popup]);
    }
}