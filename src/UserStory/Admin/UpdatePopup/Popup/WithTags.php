<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\UpdatePopup\Popup;

use Generic\Result;
use Generic\Result\Successful;
use Popups\Models\Admin\Popup;
use Popups\Models\PopupTag;
use Popups\UserStory\Admin\UpdatePopup\UpdatedPopup;
use yii\helpers\Json;

class WithTags implements UpdatedPopup
{
    private UpdatedPopup $updatedPopup;
    private array $tags;

    public function __construct(UpdatedPopup $updatedPopup, array $tags)
    {
        $this->updatedPopup = $updatedPopup;
        $this->tags = $tags;
    }

    public function result(): Result
    {
        $originResult = $this->updatedPopup->result();

        if (!$originResult->isSuccessful()) {
            return $originResult;
        }

        /** @var Popup $popup */
        $popup = $originResult->value()[0];
        $ids = $this->tags;
        PopupTag::deleteAll(['and', ['popup_id' => $popup->id], ['not in', 'tag_id', $ids]]);

        foreach ($ids as $tag) {
            $tool = PopupTag::find()->where(
                [
                    'popup_id' => $popup->id,
                    'tag_id' => $tag
                ]
            )->exists();
            if (!$tool) {
                $triggerObjectModel = new PopupTag();
                $triggerObjectModel->popup_id = $popup->id;
                $triggerObjectModel->tag_id = $tag;
                $triggerObjectModel->save();
            }
        }

        $popup->refresh();

        return new Successful([$popup]);
    }
}