<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\UpdatePopup\Popup;

use Generic\Result;
use Generic\Result\Successful;
use Popups\Models\Admin\Popup;
use Popups\Models\PopupTag;
use Popups\Models\PopupUpdatedLog;
use Popups\UserStory\Admin\UpdatePopup\UpdatedPopup;
use yii\helpers\Json;

class Log implements UpdatedPopup
{
    private UpdatedPopup $updatedPopup;
    private int $unti_id;

    public function __construct(UpdatedPopup $updatedPopup, int $unti_id)
    {
        $this->updatedPopup = $updatedPopup;
        $this->unti_id = $unti_id;
    }

    public function result(): Result
    {
        $originResult = $this->updatedPopup->result();

        if (!$originResult->isSuccessful()) {
            return $originResult;
        }

        /** @var Popup $popup */
        $popup = $originResult->value()[0];

        $triggerObjectModel = new PopupUpdatedLog();
        $triggerObjectModel->popup_id = $popup->id;
        $triggerObjectModel->unti_id = $this->unti_id;
        $triggerObjectModel->save();

        $popup->refresh();

        return new Successful([$popup]);
    }
}