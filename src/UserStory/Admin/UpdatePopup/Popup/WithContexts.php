<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\UpdatePopup\Popup;

use Generic\Result;
use Generic\Result\Successful;
use Popups\Models\Admin\Popup;
use Popups\Models\PopupContext;
use Popups\UserStory\Admin\UpdatePopup\UpdatedPopup;

class WithContexts implements UpdatedPopup
{
    private UpdatedPopup $updatedPopup;
    private array $contexts;

    public function __construct(UpdatedPopup $updatedPopup, array $contexts)
    {
        $this->updatedPopup = $updatedPopup;
        $this->contexts = $contexts;
    }

    public function result(): Result
    {
        $originResult = $this->updatedPopup->result();

        if (!$originResult->isSuccessful()) {
            return $originResult;
        }

        /** @var Popup $popup */
        $popup = $originResult->value()[0];
        $ids = $this->contexts;

        PopupContext::deleteAll(['and', ['popup_id' => $popup->id], ['not in', 'context_id', $ids]]);

        foreach ($ids as $context) {
            $tool = PopupContext::find()->where(
                [
                    'popup_id' => $popup->id,
                    'context_id' => $context
                ]
            )->exists();
            if (!$tool) {
                $triggerObjectModel = new PopupContext();
                $triggerObjectModel->popup_id = $popup->id;
                $triggerObjectModel->context_id = $context;
                $triggerObjectModel->save();
            }
        }

        $popup->refresh();

        return new Successful([$popup]);
    }
}