<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\UpdatePopup\Popup;

use Generic\Result;
use Generic\Result\Successful;
use Popups\Models\Admin\Popup;
use Popups\Models\PopupQuestion;
use Popups\UserStory\Admin\UpdatePopup\UpdatedPopup;
use Ramsey\Uuid\Uuid;
use yii\helpers\Json;

class WithQuestion implements UpdatedPopup
{
    private UpdatedPopup $updatedPopup;
    private array $questions;

    public function __construct(UpdatedPopup $updatedPopup, array $questions)
    {
        $this->updatedPopup = $updatedPopup;
        $this->questions = $questions;
    }

    public function result(): Result
    {
        $originResult = $this->updatedPopup->result();

        if (!$originResult->isSuccessful()) {
            return $originResult;
        }

        /** @var Popup $popup */
        $popup = $originResult->value()[0];
        $ids = array_column($this->questions, 'id');
        PopupQuestion::deleteAll(['and', ['popup_id' => $popup->id], ['not in', 'id', $ids]]);

        foreach ($this->questions as $k => $question) {
            if (empty($question['id'])) {
                $actionObjectModel = new PopupQuestion();
                $actionObjectModel->id = Uuid::uuid4();
                $actionObjectModel->popup_id = $popup->id;
                $actionObjectModel->title = $question['title'];
                $actionObjectModel->type = $question['type'];
                $actionObjectModel->save();
            } else {
                $actionObjectModel = PopupQuestion::findOne($question['id']);
                $actionObjectModel->title = $question['title'];
                $actionObjectModel->type = $question['type'];
                $actionObjectModel->save();
            }
        }

        $popup->refresh();

        return new Successful([$popup]);
    }
}