<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\UpdatePopup;

use Generic\Response;
use Generic\Response\Ok;
use Generic\Response\BadRequest;
use Generic\Response\ServerError;
use Infrastructure\Response\WithWarning;
use Infrastructure\UserStory;
use Infrastructure\Validation\Validatable;
use Popups\UserStory\Admin\UpdatePopup\Popup\Log;
use Popups\UserStory\Admin\UpdatePopup\Popup\Persistent;
use Popups\UserStory\Admin\UpdatePopup\Popup\WithCondition;
use Popups\UserStory\Admin\UpdatePopup\Popup\WithContexts;
use Popups\UserStory\Admin\UpdatePopup\Popup\WithQuestion;
use Popups\UserStory\Admin\UpdatePopup\Popup\WithTags;
use Ramsey\Uuid\Uuid;
use Yii;

class UpdatePopup implements UserStory
{
    private Validatable $validatable;
    private string $id;
    private int $unti_id;

    public function __construct(string $id, Validatable $validatable, int $userId)
    {
        $this->id = $id;
        $this->validatable = $validatable;
        $this->unti_id = $userId;
    }

    public function response(): Response
    {
        $validationResult = $this->validatable->result();

        if (!$validationResult->isSuccessful()) {
            return new WithWarning(new BadRequest($validationResult->error()));
        }

        $validation = $validationResult->value();

        $result =
            (new Log(
                new WithQuestion(
                    new WithCondition(
                        new WithTags(
                            new WithContexts(
                                new Persistent(
                                    $this->id,
                                    $validation
                                ),
                                $validation['contexts'],
                            ),
                            $validation['tags'],
                        ),
                        $validation['conditions'] ?? [],
                    ),
                    $validation['questions'] ?? [],
                ),
                $this->unti_id
            ))
                ->result();

        if (!$result->isSuccessful()) {
            Yii::error($result->error());

            return new ServerError();
        }

        return
            new Ok(
                $result->value()[0]->toArray()
            );
    }
}