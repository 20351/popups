<?php

declare(strict_types=1);

namespace Popups\UserStory\Admin\UpdatePopup;

use Generic\Result;

interface UpdatedPopup
{
    public function result(): Result;
}