<?php

declare(strict_types=1);

namespace Popups\Request\User;


use Http\Request;
use Http\Request\Method;
use Http\Request\Method\Get;
use Http\Request\Url;
use Http\Request\Url\FromString;
use Infrastructure\Environment\Env;

class GetContext implements Request
{
    private string $contextId;

    public function __construct(string $contextId)
    {
        $this->contextId = $contextId;
    }

    public function url(): Url
    {
        return
            new FromString(
                sprintf(
                    '%s/v1/context/%s/',
                    (new Env('USER_API_URL'))->value(),
                    $this->contextId,
                )
            );
    }

    public function headers(): array
    {
        return [
            'Content-type' => 'application/json',
            'Authorization' => sprintf(
                'Token %s',
                (new Env('USER_API_TOKEN'))->value()
            ),
        ];
    }

    public function body(): string
    {
        return '';
    }

    public function method(): Method
    {
        return new Get();
    }
}
