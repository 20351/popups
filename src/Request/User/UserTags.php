<?php

declare(strict_types=1);

namespace Popups\Request\User;

use Http\Request;
use Http\Request\Method;
use Http\Request\Method\Get;
use Http\Request\Url;
use Http\Request\Url\FromString;
use Infrastructure\Environment\Env;
use Infrastructure\Environment\UrlEnv;
use yii\helpers\Json;

class UserTags implements Request
{
    private int $untiId;

    public function __construct(int $untiId)
    {
        $this->untiId = $untiId;
    }

    public function method(): Method
    {
        return  new Get();
    }

    public function url(): Url
    {
        return
            new FromString(
                sprintf(
                    '%s/v1/user/%d/',
                    (new UrlEnv('USER_API_URL'))->value(),
                    $this->untiId
                )
            );
    }

    public function headers(): array
    {
        return [
            'Content-type' => 'application/json',
            'Authorization' => sprintf(
                'Token %s',
                (new Env('USER_API_TOKEN'))->value()
            )
        ];
    }

    public function body(): string
    {
        return '';
    }
}